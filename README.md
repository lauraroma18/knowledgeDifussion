# Degree Project

In this project, an emotional model proposed by Albert Mehrabian is integrated with an evolutionary model based on agents to implement a prototype simulation of knowledge diffusion between emotional agents in a virtual grid environment type 2D. The prototype is a multi-agent system, in which two single-reflex agents called the predator agent and the feeding zone agent interact. There are also two agents based on models called explorer agent and settler agent, which share the internal structure design but vary in the interaction with the environment; the explorer agents are in charge of the knowledge diffusion dynamics; these last two types of agents are considered emotional agents, since they are configured to have an emotion and a personality emulated through the emotional model, these two elements allow simulating the interaction and decision making.

Finally, we sought to evaluate the behavior of the system in terms of reproduction and evolution, in order to determine whether sharing information from its environment could be considered an evolutionary advantage for emotional agents.


[Diagram of use cases (agents)](https://drive.google.com/file/d/1wKJp0cW7KekC0SzLkf1jbLAh3ANJJNd5/view?usp=sharing)

**Made by:**
* Laura L. Romero Martínez
* Juan M. Sanchez Villada

# Guide for reading the file code GUI.py

Each section such as imports, variables, GUI loading and functions at the beginning have a comment indicating the passage from one to another. Additionally each of the functions and variables have descriptive names about their role within the coding in order to facilitate the reading, most of the functions have a description of their functionality, some have specified the role of their input and output parameters, as well as internal comments in some algorithms that require it for the computation being performed.

The order is as follows:

* In the first section we find the libraries imported during the implementation.
* The declaration of variables.
* Loading of the GUI with map textures, inputs, panels, buttons etc.
* The methods for the GUI functionality such as map construction, button actions (click-hoover), input text management methods etc. are created.
* In the next section all the functions involved in the simulation are derived, such as reproduction, displacement, emotion calculation, k-means management for the grouping by tribes, prisoner's dilemma within the negotiation between individuals, feeding, predator actions, knowledge diffusion among others and towards the end of the list of functions we find a last function called "main" which is the body of the whole simulation, it is responsible for integrating the different functionalities involved in the simulation of each generation.


# Installation on your local machine
The executable files can be found in the following section of the repository:

[https://gitlab.com/lauraroma18/knowledgeDifussion/-/tree/test/main/Ejecutables](https://gitlab.com/lauraroma18/knowledgeDifussion/-/tree/test/main/Ejecutables)

Enter the appropriate folder for your operating system (linux derived from ubuntu or windows), then go to the dist folder and open the main folder, inside this folder you will find the main file (for linux) and main.exe (for windows), just click on it and the application will open.

# Attachments

The attachments folder contains several folders internally, this folder can be found in the following link:

[Link Tests and Reports](https://drive.google.com/drive/folders/1g71455pHW5MZmxLK_C4D7swxFHmEKa17?usp=sharing)


**Folder pruebas:** Each test scenario has a folder assigned with the file data-x.txt containing the outputs of the simulation, the file prueba-x.txt are the input variables of the scenario, finally, the file observation.txt, stores relevant data obtained by observation during its execution.

**Foler reportes:**

1. Regresion.xlsx: The SPSS and Spearman's Rho sheets contain the correlation coefficients and significance of applying Spearman's test, and the Kolmogorov-Smirnov sheet has the statistics that allow us to affirm that the variables of the dataset are not symmetric.

2. Totalizados.xlsx: The sheet aparicionZonas, is the evidence of the simulations carried out to obtain the test values of the variables of predator occurrence rate and feeding zones. The totalizados sheet is the preprocessed dataset with the scenario data consolidated by the statistical measure of the median. And the report sheet was the dataset evaluated and analyzed in ch. 6.

3. Reporte-TG.html: This is the report returned by the panda profiling library.



