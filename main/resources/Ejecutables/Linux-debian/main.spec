# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['main.py'],
             pathex=['/media/lauraroma/0A8423BE06D94565/Trabajo de Grado/trabajo-de-grado/main'],
             binaries=[],
             datas=[('./resources/fuentes/*ttf', 'resources/fuentes'),
             	('./resources/imagenes/*jpg', 'resources/imagenes'),
             	('./resources/imagenes/configuracion/*png', 'resources/imagenes/configuracion'),
             	('./resources/imagenes/ingresar-parametros/*png', 'resources/imagenes/ingresar-parametros'),
             	('./resources/imagenes/inicio/*png', 'resources/imagenes/inicio'),
             	('./resources/imagenes/simulacion/*png', 'resources/imagenes/simulacion'),
             	('./resources/mapas/*png', 'resources/mapas')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='main',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='main')
