import sys
import pygame
from pygame.locals import *
import time
import os
import random
import operator
import math
import easygui as eg
import datetime

textboxGroup = pygame.sprite.OrderedUpdates()


class GUI:
    
    '''
                --------------------- Declaración de variables  ---------------------
    '''
    download = True
    # centra la ventana
    os.environ['SDL_VIDEO_CENTERED'] = '1'

    # Se inicializa la libreria PyGame
    pygame.init()
    # Variables para la creación y el despliegue de la ventana
    width = 1200
    height = 651
    gray = (181, 221, 253)
    ventana = pygame.display.set_mode((1180, height), 0, 0)
    ventana.fill(gray)
    input_individuos = []
    input_depredadores = []
    input_alimentacion = []
    #almacena todos los input del panel entorno
    input_entorno = []
    pause_simulacion = True
    velocidad = 2000
    velocidad_pause = 0
    generacion = 0

    dimension = 0
    mapa = []

    
    current_path = os.path.dirname(__file__) # Where your .py file is located
    resource_path = os.path.join(current_path, 'resources') # The resource folder path
    image_path = os.path.join(resource_path, 'imagenes') # The image folder path
    mapas_path = os.path.join(resource_path, 'mapas') # The mapas folder 
    fuentes_path = os.path.join(resource_path, 'fuentes') # The mapas folder 
    

    # Definición del nombre e icono de la ventana
    pygame.display.set_caption("Difusión de Conocimiento")
    icon = pygame.image.load(os.path.join(image_path, 'univalle.jpg'))
    pygame.display.set_icon(icon)

    # Listados de los botones de los paneles
    lista_btn_opciones = []
    lista_btn_entorno = []
    lista_btn_individuo = []
    lista_btn_alimentacion = []
    lista_btn_simulacion = []
    lista_btn_depredador = []
    lista_btn_inicio = []


    # Variables auxiliares
    recursosIniciales = True
    matrizdispersa = []
    individuosIniciales = True
    maximo_exploradores = 5

    tasaAparicionZonas = 0
    tasaAparicionDepredadores = 0

    # Variables globales
    individuosVivos = 0
    individuosMuertos = 0
    
    """ Array Individuos """
    individuos_array = []
    hijos_array = []
    idIndividuos = 0

    """ Array  Zonas de alimentacion """
    zonasAlimentacion_array = []
    idZonaAlimentacion = 0

    """ Array Depredadores """
    depredadores_array = []
    idDepredadores = 0

    # ******* Configuracion para los text field *******
    fieldActivo = False
    textoDigitado = ""
    white = (255, 255, 255)
    black = (0, 0, 0)

    """ Diccionario con los valores de las variables de control de la simulacion """
    varibales_control = {'time_life': 0, # Max: 20  Min: 10 --> por desplazamiento del mapa
                            # numero de hijos que puedo tener durante toda la vida       
                         'reproducciones': 0, # Max:5   Min: 1  un cuarto de las casillas para reproducirse
                            #tiempo que debe esperar para volver a tener un hijo
                         'tespera': 0,  # formula: MAX:10 min:4 time_life/reproducciones
                         'talimentacion': 0, # Max: 5  (20/4 1 cuarto de su vida se alimentan)   Min: 1 proporción puede ser entre tiempo de vida y reproduciones
                         'tasa_mutacion': 10, # fija 10%
                         # Variables de los individuos
                         'numero_aparicion': 0, # vida: time_life/2 max:10 min=5 
                         # Variable del depredador
                         'tasas_aparicion': 0, # mirar justificacion min:3 max:7 de la tabla elegimos los valores que más se repiten
                         'cantidad_recursos': 0, # Max: 42(mermar)  Min: 17  cantidad población min=20% max=50%--> 1 generación su capacidad limite  de alimentacionse acaba
                         # Variables de las zonas
                         'dispersion_inicial': 2, # fija = 2
                         'tamano_poblacion': 0,# Max: 17  Min: 8 (MAX que carga-> 87) ley pareto
                         'tamano_grupos': 4, # Min: 4 fija
                         'prob_explorador': 0, # Max: 50  Min: 20
                         }

    """ 
        Diccionario para almacenar valores que permiten un mejor estudio de cada simulación 
    """
    variables_test = {'individuosVivos': 0,
                    'individuosMuertos': 0,
                    'depredadores': 0,
                    'zonasAlimentacion': 0,
                    'neutralMuertos': 0,
                    'neutralVivos': 0,
                    'neutralConocimientoZonas': 0,
                    'neutralConocimientoDepredadores': 0,
                    'neutralFelicidadH': 0,
                    'neutralFelicidadM': 0,
                    'neutralIraH': 0,
                    'neutralIraM': 0,
                    'neutralMiedoH': 0,
                    'neutralMiedoM': 0,
                    'neutralEsperanzaH': 0,
                    'neutralEsperanzaM': 0,
                    'optimistasMuertos': 0,
                    'optimistasVivos': 0,
                    'optimistaConocimientoZonas': 0,
                    'optimistaConocimientoDepredadores': 0,
                    'optimistasFelicidadH': 0,
                    'optimistasFelicidadM': 0,
                    'optimistasIraH': 0,
                    'optimistasIraM': 0,
                    'optimistasMiedoH': 0,
                    'optimistasMiedoM': 0,
                    'optimistasEsperanzaH': 0,
                    'optimistasEsperanzaM': 0,
                    'muyOptimistasMuertos': 0,
                    'muyOptimistasVivos': 0,
                    'muyOptimistaConocimientoZonas': 0,
                    'muyOptimistaConocimientoDepredadores': 0,
                    'muyOptimistasFelicidadH': 0,
                    'muyOptimistasFelicidadM': 0,
                    'muyOptimistasIraH': 0,
                    'muyOptimistasIraM': 0,
                    'muyOptimistasMiedoH': 0,
                    'muyOptimistasMiedoM': 0,
                    'muyOptimistasEsperanzaH': 0,
                    'muyOptimistasEsperanzaM': 0,
                    'pesimistasMuertos': 0,
                    'pesimistasVivos': 0,
                    'pesimistaConocimientoZonas': 0,
                    'pesimistaConocimientoDepredadores': 0,
                    'pesimistasFelicidadH': 0,
                    'pesimistasFelicidadM': 0,
                    'pesimistasIraH': 0,
                    'pesimistasIraM': 0,
                    'pesimistasMiedoH': 0,
                    'pesimistasMiedoM': 0,
                    'pesimistasEsperanzaH': 0,
                    'pesimistasEsperanzaM': 0,
                    'muyPesimistasMuertos': 0,
                    'muyPesimistasVivos': 0,
                    'MuyPesimistaConocimientoZonas': 0,
                    'MuyPesimistaConocimientoDepredadores': 0,
                    'muyPesimistasFelicidadH': 0,
                    'muyPesimistasFelicidadM': 0,
                    'muyPesimistasIraH': 0,
                    'muyPesimistasIraM': 0,
                    'muyPesimistasMiedoH': 0,
                    'muyPesimistasMiedoM': 0,
                    'muyPesimistasEsperanzaH': 0,
                    'muyPesimistasEsperanzaM': 0,
                    'Panico_Depredador': 0,
                    'Panico_Comida': 0,
                    'Tipo_1': 0,
                    'Tipo_2': 0,
                    'Agruparse': 0,
                    'Aislarse': 0,
    }

    ''' Para almacenar los datos que se guardan al final de cada simulación'''
    data_set = []

    
    #    --------------------------- Diccionario texturas del mapa --------------------
    
    texturas = {
        'grass': os.path.join(mapas_path, "grass.png"),
        'water': os.path.join(mapas_path, "water.png"),
        'depredador': os.path.join(mapas_path, "depredador.png"),
        'explorador': os.path.join(mapas_path, "explorador.png"),
        # Feliz
        'felizpc': os.path.join(mapas_path, "Feliz-panico-comida.png"),
        'felizpcm': os.path.join(mapas_path, "Feliz-panico-comidaM.png"),
        'felizpd': os.path.join(mapas_path, "Feliz-panico-depredador.png"),
        'felizpdm': os.path.join(mapas_path, "Feliz-panico-depredadorM.png"),
        'feliza': os.path.join(mapas_path, "Feliz-agrupamiento.png"),
        'felizam': os.path.join(mapas_path, "Feliz-agrupamientoM.png"),
        'felizais': os.path.join(mapas_path, "Feliz-agrupamiento-aislamiento.png"),
        'felizaism': os.path.join(mapas_path, "Feliz-agrupamiento-aislamientoM.png"),
        'felizdt1': os.path.join(mapas_path, "Feliz-desplazamiento-tipo1.png"),
        'felizdt1m': os.path.join(mapas_path, "Feliz-desplazamiento-tipo1M.png"),
        'felizdt2': os.path.join(mapas_path, "Feliz-desplazamiento-tipo2.png"),
        'felizdt2m': os.path.join(mapas_path, "Feliz-desplazamiento-tipo2M.png"),
        # Esperanza
        'esperanzapc': os.path.join(mapas_path, "Esperanza-panico-comida.png"),
        'esperanzapcm': os.path.join(mapas_path, "Esperanza-panico-comidaM.png"),
        'esperanzapd': os.path.join(mapas_path, "Esperanza-panico-depredador.png"),
        'esperanzapdm': os.path.join(mapas_path, "Esperanza-panico-depredadorM.png"),
        'esperanzaa': os.path.join(mapas_path, "Esperanza-agrupamiento.png"),
        'esperanzaam': os.path.join(mapas_path, "Esperanza-agrupamientoM.png"),
        'esperanzaais': os.path.join(mapas_path, "Esperanza-agrupamiento-aislamiento.png"),
        'esperanzaaism': os.path.join(mapas_path, "Esperanza-agrupamiento-aislamientoM.png"),
        'esperanzadt1': os.path.join(mapas_path, "Esperanza-desplazamiento-tipo1.png"),
        'esperanzadt1m': os.path.join(mapas_path, "Esperanza-desplazamiento-tipo1M.png"),
        'esperanzadt2': os.path.join(mapas_path, "Esperanza-desplazamiento-tipo2.png"),
        'esperanzadt2m': os.path.join(mapas_path, "Esperanza-desplazamiento-tipo2M.png"),
        # Ira
        'irapc': os.path.join(mapas_path, "Ira-panico-comida.png"),
        'irapcm': os.path.join(mapas_path, "Ira-panico-comidaM.png"),
        'irapd': os.path.join(mapas_path, "Ira-panico-depredador.png"),
        'irapdm': os.path.join(mapas_path, "Ira-panico-depredadorM.png"),
        'iraa': os.path.join(mapas_path, "Ira-agrupamiento.png"),
        'iraam': os.path.join(mapas_path, "Ira-agrupamientoM.png"),
        'iraais': os.path.join(mapas_path, "Ira-agrupamiento-aislamiento.png"),
        'iraaism': os.path.join(mapas_path, "Ira-agrupamiento-aislamientoM.png"),
        'iradt1': os.path.join(mapas_path, "Ira-desplazamiento-tipo1.png"),
        'iradt1m': os.path.join(mapas_path, "Ira-desplazamiento-tipo1M.png"),
        'iradt2': os.path.join(mapas_path, "Ira-desplazamiento-tipo2.png"),
        'iradt2m': os.path.join(mapas_path, "Ira-desplazamiento-tipo2M.png"),
        # miedo
        'miedopc': os.path.join(mapas_path, "Miedo-panico-comida.png"),
        'miedopcm': os.path.join(mapas_path, "Miedo-panico-comidaM.png"),
        'miedopd': os.path.join(mapas_path, "Miedo-panico-depredador.png"),
        'miedopdm': os.path.join(mapas_path, "Miedo-panico-depredadorM.png"),
        'miedoa': os.path.join(mapas_path, "Miedo-agrupamiento.png"),
        'miedoam': os.path.join(mapas_path, "Miedo-agrupamientoM.png"),
        'miedoais': os.path.join(mapas_path, "Miedo-agrupamiento-aislamiento.png"),
        'miedoaism': os.path.join(mapas_path, "Miedo-agrupamiento-aislamientoM.png"),
        'miedodt1': os.path.join(mapas_path, "Miedo-desplazamiento-tipo1.png"),
        'miedodt1m': os.path.join(mapas_path, "Miedo-desplazamiento-tipo1M.png"),
        'miedodt2': os.path.join(mapas_path, "Miedo-desplazamiento-tipo2.png"),
        'miedodt2m': os.path.join(mapas_path, "Miedo-desplazamiento-tipo2M.png"),
    }


    '''
                ---------------------Cargar Imágenes utilizadas en el prototipo ---------------------
    '''
  

    #           --------------------- INPUTS DE TODOS LOS PANELES ----------------------------
    # Input able

    # .convert_alpha() optimiza las imágenes cambiandolas al tipo de pixel de la pantalla
    imagen_input_able = pygame.image.load(os.path.join(image_path, 'ingresar-parametros/input-able.png')).convert_alpha()
    imagen_input_able = pygame.transform.scale(imagen_input_able, (177, 34))

    # Input enable
    imagen_input_enable = pygame.image.load(os.path.join(image_path, "ingresar-parametros/input-enable.png")).convert_alpha()
    imagen_input_enable = pygame.transform.scale(imagen_input_enable, (177, 34))

    # Input check
    imagen_input_check = pygame.image.load(os.path.join(image_path, "ingresar-parametros/input-check.png")).convert_alpha()
    imagen_input_check = pygame.transform.scale(imagen_input_check, (177, 34))
    
    #           ---------------------- PANEL INICIO--------------------------
    # background
    imagen_panel_inicio = pygame.image.load(os.path.join(image_path, "inicio/inicio.png")).convert_alpha() 
    imagen_panel_inicio = pygame.transform.scale(imagen_panel_inicio, (1200, 650))

    # Boton next
    imagen_btn_next_inicio = pygame.image.load(os.path.join(image_path, "inicio/btn-next.png")).convert_alpha()
    imagen_btn_next_inicio = pygame.transform.scale(imagen_btn_next_inicio, (88, 38))
    
    # Boton next hoover
    imagen_btn_next_inicio_hoover = pygame.image.load(os.path.join(image_path, "inicio/btn-next-hoover.png")).convert_alpha()
    imagen_btn_next_inicio_hoover = pygame.transform.scale(imagen_btn_next_inicio_hoover, (88, 38))
    

    #           ---------------------- PANEL OPCIONES CONFIGURACION --------------------------

    
    # background
    imagen_panel_configuracion = pygame.image.load(os.path.join(image_path, "configuracion/Configuracion1.png")).convert_alpha()
    imagen_panel_configuracion = pygame.transform.scale(imagen_panel_configuracion, (549, height))
    
    # Boton digitar
    imagen_btn_digitar = pygame.image.load(os.path.join(image_path, "configuracion/btn-digitar.png")).convert_alpha() 
    imagen_btn_digitar = pygame.transform.scale(imagen_btn_digitar, (147, 146))
    
    # Boton cargar
    imagen_btn_cargar = pygame.image.load(os.path.join(image_path, "configuracion/btn-cargar.png")).convert_alpha() 
    imagen_btn_cargar = pygame.transform.scale(imagen_btn_cargar, (147, 146))
    
    # Boton digitar hoover
    imagen_btn_digitar_hoover = pygame.image.load(os.path.join(image_path, "configuracion/btn-digitar-hoover.png")).convert_alpha()
    imagen_btn_digitar_hoover = pygame.transform.scale(imagen_btn_digitar_hoover, (147, 146))
    
    # Boton cargar hoover
    imagen_btn_cargar_hoover = pygame.image.load(os.path.join(image_path, "configuracion/btn-cargar-hoover.png")).convert_alpha() 
    imagen_btn_cargar_hoover = pygame.transform.scale(imagen_btn_cargar_hoover, (147, 146))
    

    #---------------------- PANEL ENTORNO  --------------------------

    # background
    imagen_panel_entorno = pygame.image.load(os.path.join(image_path, "ingresar-parametros/Panel-entorno.png")).convert_alpha() 
    imagen_panel_entorno = pygame.transform.scale(imagen_panel_entorno, (549, height))
    
    # Botón back
    imagen_btn_back_entorno = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-back.png")).convert_alpha() 
    imagen_btn_back_entorno = pygame.transform.scale(imagen_btn_back_entorno, (30, 30))
    
    # Botón back hoover
    imagen_btn_back_entorno_hoover = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-back-hoover.png")).convert_alpha() 
    imagen_btn_back_entorno_hoover = pygame.transform.scale(imagen_btn_back_entorno_hoover, (30, 30))

    # Botón next
    imagen_btn_next_entorno = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-next.png")).convert_alpha() 
    imagen_btn_next_entorno = pygame.transform.scale(imagen_btn_next_entorno, (48, 48))
    
    # Botón next hoover
    imagen_btn_next_entorno_hoover = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-next-hoover.png")).convert_alpha() 
    imagen_btn_next_entorno_hoover = pygame.transform.scale(imagen_btn_next_entorno_hoover, (48, 48))
    
    
    #---------------------- PANEL INDIVIDUO  --------------------------

    # background
    imagen_panel_individuo = pygame.image.load(os.path.join(image_path, "ingresar-parametros/Panel-individuo.png")).convert_alpha() 
    imagen_panel_individuo = pygame.transform.scale(imagen_panel_individuo, (549, height))
    
    # Botón back
    imagen_btn_back_individuo = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-back.png")).convert_alpha() 
    imagen_btn_back_individuo = pygame.transform.scale(imagen_btn_back_individuo, (30, 30))
    
    # Botón back hoover
    imagen_btn_back_individuo_hoover = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-back-hoover.png")) 
    imagen_btn_back_individuo_hoover = pygame.transform.scale(imagen_btn_back_individuo_hoover, (30, 30))

    # Botón next
    imagen_btn_next_individuo = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-next.png")).convert_alpha() 
    imagen_btn_next_individuo = pygame.transform.scale(imagen_btn_next_individuo, (48, 48))
    
    # Botón back hoover
    imagen_btn_next_individuo_hoover = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-next-hoover.png")).convert_alpha()
    imagen_btn_next_individuo_hoover = pygame.transform.scale(imagen_btn_next_individuo_hoover, (48, 48))
    

    #---------------------- PANEL ALIMENTACION  --------------------------

    # background
    imagen_panel_alimentacion = pygame.image.load(os.path.join(image_path, "ingresar-parametros/Panel-alimentacion.png")).convert_alpha() 
    imagen_panel_alimentacion = pygame.transform.scale(imagen_panel_alimentacion, (549, height))
    
    # Botón back
    imagen_btn_back_alimentacion = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-back.png")).convert_alpha() 
    imagen_btn_back_alimentacion = pygame.transform.scale(imagen_btn_back_alimentacion, (30, 30))
    
    # Botón back hoover
    imagen_btn_back_alimentacion_hoover = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-back-hoover.png")).convert_alpha() 
    imagen_btn_back_alimentacion_hoover = pygame.transform.scale(imagen_btn_back_alimentacion_hoover, (30, 30))

    # Botón next
    imagen_btn_next_alimentacion = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-next.png")).convert_alpha() 
    imagen_btn_next_alimentacion = pygame.transform.scale(imagen_btn_next_alimentacion, (48, 48))
    
    # Botón back hoover
    imagen_btn_next_alimentacion_hoover = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-next-hoover.png")).convert_alpha() 
    imagen_btn_next_alimentacion_hoover = pygame.transform.scale(imagen_btn_next_alimentacion_hoover, (48, 48))
    

    #---------------------- PANEL DEPREDADOR  --------------------------

    # background
    imagen_panel_depredador = pygame.image.load(os.path.join(image_path, "ingresar-parametros/Panel-depredador.png")).convert_alpha() 
    imagen_panel_depredador = pygame.transform.scale(imagen_panel_depredador, (549, height))
    
    # Botón back
    imagen_btn_back_depredador = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-back.png")).convert_alpha() 
    imagen_btn_back_depredador = pygame.transform.scale(imagen_btn_back_depredador, (30, 30))
    
    # Botón back hoover
    imagen_btn_back_depredador_hoover = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-back-hoover.png")).convert_alpha() 
    imagen_btn_back_depredador_hoover = pygame.transform.scale(imagen_btn_back_depredador_hoover, (30, 30))

    # Botón next
    imagen_btn_next_depredador = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-next.png")).convert_alpha() 
    imagen_btn_next_depredador = pygame.transform.scale(imagen_btn_next_depredador, (48, 48))
    
    # Botón back hoover
    imagen_btn_next_depredador_hoover = pygame.image.load(os.path.join(image_path, "ingresar-parametros/btn-next-hoover.png")).convert_alpha() 
    imagen_btn_next_depredador_hoover = pygame.transform.scale(imagen_btn_next_depredador_hoover, (48, 48))


    #---------------------- PANEL SIMULACIÓN  --------------------------

    # background
    imagen_panel_simulacion = pygame.image.load(os.path.join(image_path, "simulacion/panel-simulacion.png")).convert_alpha() 
    imagen_panel_simulacion = pygame.transform.scale(imagen_panel_simulacion, (549, height))

    # Botón pausar
    imagen_btn_pause = pygame.image.load(os.path.join(image_path, "simulacion/pause.png")).convert_alpha()
    imagen_btn_pause = pygame.transform.scale(imagen_btn_pause, (80,80))
    
    # Botón pausar hoover
    imagen_btn_pause_hoover = pygame.image.load(os.path.join(image_path, "simulacion/pause-hoover.png")).convert_alpha()
    imagen_btn_pause_hoover = pygame.transform.scale(imagen_btn_pause_hoover, (80,80))

    # Botón velocidad aumentar
    imagen_btn_aumentar = pygame.image.load(os.path.join(image_path, "simulacion/aumentar.png")).convert_alpha()
    imagen_btn_aumentar = pygame.transform.scale(imagen_btn_aumentar, (60, 60))
   
    # Botón velocidad aumentar hoover
    imagen_btn_aumentar_hoover = pygame.image.load(os.path.join(image_path, "simulacion/aumentar-hoover.png")).convert_alpha()
    imagen_btn_aumentar_hoover = pygame.transform.scale(imagen_btn_aumentar_hoover, (60, 60))

    # Botón velocidad disminuir
    imagen_btn_disminuir = pygame.image.load(os.path.join(image_path, "simulacion/disminuir.png")).convert_alpha()
    imagen_btn_disminuir = pygame.transform.scale(imagen_btn_disminuir, (60, 60))
    
    # Botón velocidad disminuir hoover
    imagen_btn_disminuir_hoover = pygame.image.load(os.path.join(image_path, "simulacion/disminuir-hoover.png")).convert_alpha()
    imagen_btn_disminuir_hoover = pygame.transform.scale(imagen_btn_disminuir_hoover, (60, 60))

    # Botón detener
    imagen_btn_detener = pygame.image.load(os.path.join(image_path, "simulacion/detener.png")).convert_alpha()
    imagen_btn_detener = pygame.transform.scale(imagen_btn_detener, (60, 60))
    
    # Botón detener hoover
    imagen_btn_detener_hoover = pygame.image.load(os.path.join(image_path, "simulacion/detener-hoover.png")).convert_alpha()
    imagen_btn_detener_hoover = pygame.transform.scale(imagen_btn_detener_hoover, (60, 60))
    
    # Botón reiniciar
    imagen_btn_reiniciar = pygame.image.load(os.path.join(image_path, "simulacion/reiniciar.png")).convert_alpha()
    imagen_btn_reiniciar = pygame.transform.scale(imagen_btn_reiniciar, (60, 60))
   
    # Botón reiniciar hoover
    imagen_btn_reiniciar_hoover = pygame.image.load(os.path.join(image_path, "simulacion/reiniciar-hoover.png")).convert_alpha()
    imagen_btn_reiniciar_hoover = pygame.transform.scale(imagen_btn_reiniciar_hoover, (60, 60)) 
    
    
    # Botón play
    imagen_btn_play = pygame.image.load(os.path.join(image_path, "simulacion/play.png")).convert_alpha()
    imagen_btn_play = pygame.transform.scale(imagen_btn_play, (80,80))
    
    # Botón play hoover
    imagen_btn_play_hoover = pygame.image.load(os.path.join(image_path, "simulacion/play-hoover.png")).convert_alpha()
    imagen_btn_play_hoover = pygame.transform.scale(imagen_btn_play_hoover, (80,80))

    #----------------------      MAPA        --------------------------

    # pasto
    imagen_grass = pygame.image.load(texturas['grass']).convert_alpha()
    imagen_grass = pygame.transform.scale(imagen_grass, (31, 31))
    # agua
    imagen_water = pygame.image.load(texturas['water']).convert_alpha()
    imagen_water = pygame.transform.scale(imagen_water, (31, 31))
    # depredador
    imagen_predator = pygame.image.load(texturas['depredador']).convert_alpha()
    imagen_predator = pygame.transform.scale(imagen_predator, (31, 31))
    # explorador
    imagen_explorador = pygame.image.load(texturas['explorador']).convert_alpha()
    imagen_explorador = pygame.transform.scale(imagen_explorador, (31, 31))
    # Feliz panico comida
    imagen_felizpc = pygame.image.load(texturas['felizpc']).convert_alpha()
    imagen_felizpc = pygame.transform.scale(imagen_felizpc, (31, 31))
    imagen_felizpcm = pygame.image.load(texturas['felizpcm'])
    imagen_felizpcm = pygame.transform.scale(imagen_felizpcm, (31, 31))
    # Feliz panico depredador
    imagen_felizpd = pygame.image.load(texturas['felizpd']).convert_alpha()
    imagen_felizpd = pygame.transform.scale(imagen_felizpd, (31, 31))
    imagen_felizpdm = pygame.image.load(texturas['felizpdm']).convert_alpha()
    imagen_felizpdm = pygame.transform.scale(imagen_felizpdm, (31, 31))
    # Feliz agrupamiento
    imagen_feliza = pygame.image.load(texturas['feliza']).convert_alpha()
    imagen_feliza = pygame.transform.scale(imagen_feliza, (31, 31))
    imagen_felizam = pygame.image.load(texturas['felizam']).convert_alpha()
    imagen_felizam = pygame.transform.scale(imagen_felizam, (31, 31))
    # Feliz agrupamiento aislamiento
    imagen_felizais = pygame.image.load(texturas['felizais']).convert_alpha()
    imagen_felizais = pygame.transform.scale(imagen_felizais, (31, 31))
    imagen_felizaism = pygame.image.load(texturas['felizaism'])
    imagen_felizaism = pygame.transform.scale(imagen_felizaism, (31, 31))
    # Feliz desplazamiento tipo1
    imagen_felizdt1 = pygame.image.load(texturas['felizdt1']).convert_alpha()
    imagen_felizdt1 = pygame.transform.scale(imagen_felizdt1, (31, 31))
    imagen_felizdt1m = pygame.image.load(texturas['felizdt1m'])
    imagen_felizdt1m = pygame.transform.scale(imagen_felizdt1m, (31, 31))
    # Feliz desplazamiento tipo2
    imagen_felizdt2 = pygame.image.load(texturas['felizdt2']).convert_alpha()
    imagen_felizdt2 = pygame.transform.scale(imagen_felizdt2, (31, 31))
    imagen_felizdt2m = pygame.image.load(texturas['felizdt2m']).convert_alpha()
    imagen_felizdt2m = pygame.transform.scale(imagen_felizdt2m, (31, 31))

    # Esperanza panico comida
    imagen_esperanzapc = pygame.image.load(texturas['esperanzapc']).convert_alpha()
    imagen_esperanzapc = pygame.transform.scale(imagen_esperanzapc, (31, 31))
    imagen_esperanzapcm = pygame.image.load(texturas['esperanzapcm']).convert_alpha()
    imagen_esperanzapcm = pygame.transform.scale(imagen_esperanzapcm, (31, 31))
    # Esperanza panico depredador
    imagen_esperanzapd = pygame.image.load(texturas['esperanzapd']).convert_alpha()
    imagen_esperanzapd = pygame.transform.scale(imagen_esperanzapd, (31, 31))
    imagen_esperanzapdm = pygame.image.load(texturas['esperanzapdm']).convert_alpha()
    imagen_esperanzapdm = pygame.transform.scale(imagen_esperanzapdm, (31, 31))
    # Esperanza agrupamiento
    imagen_esperanzaa = pygame.image.load(texturas['esperanzaa']).convert_alpha()
    imagen_esperanzaa = pygame.transform.scale(imagen_esperanzaa, (31, 31))
    imagen_esperanzaam = pygame.image.load(texturas['esperanzaam']).convert_alpha()
    imagen_esperanzaam = pygame.transform.scale(imagen_esperanzaam, (31, 31))
    # Esperanza agrupamiento aislamiento
    imagen_esperanzaais = pygame.image.load(texturas['esperanzaais']).convert_alpha()
    imagen_esperanzaais = pygame.transform.scale(imagen_esperanzaais, (31, 31))
    imagen_esperanzaaism = pygame.image.load(texturas['esperanzaaism']).convert_alpha()
    imagen_esperanzaaism = pygame.transform.scale(
        imagen_esperanzaaism, (31, 31))
    # Esperanza desplazamiento tipo1
    imagen_esperanzadt1 = pygame.image.load(texturas['esperanzadt1']).convert_alpha()
    imagen_esperanzadt1 = pygame.transform.scale(imagen_esperanzadt1, (31, 31))
    imagen_esperanzadt1m = pygame.image.load(texturas['esperanzadt1m']).convert_alpha()
    imagen_esperanzadt1m = pygame.transform.scale(
        imagen_esperanzadt1m, (31, 31))
    # Esperanza desplazamiento tipo2
    imagen_esperanzadt2 = pygame.image.load(texturas['esperanzadt2']).convert_alpha()
    imagen_esperanzadt2 = pygame.transform.scale(imagen_esperanzadt2, (31, 31))
    imagen_esperanzadt2m = pygame.image.load(texturas['esperanzadt2m']).convert_alpha()
    imagen_esperanzadt2m = pygame.transform.scale(
        imagen_esperanzadt2m, (31, 31))

    # Ira panico comida
    imagen_irapc = pygame.image.load(texturas['irapc']).convert_alpha()
    imagen_irapc = pygame.transform.scale(imagen_irapc, (31, 31))
    imagen_irapcm = pygame.image.load(texturas['irapcm']).convert_alpha()
    imagen_irapcm = pygame.transform.scale(imagen_irapcm, (31, 31))
    # Ira panico depredador
    imagen_irapd = pygame.image.load(texturas['irapd']).convert_alpha()
    imagen_irapd = pygame.transform.scale(imagen_irapd, (31, 31))
    imagen_irapdm = pygame.image.load(texturas['irapdm']).convert_alpha()
    imagen_irapdm = pygame.transform.scale(imagen_irapdm, (31, 31))
    # Ira agrupamiento
    imagen_iraa = pygame.image.load(texturas['iraa']).convert_alpha()
    imagen_iraa = pygame.transform.scale(imagen_iraa, (31, 31))
    imagen_iraam = pygame.image.load(texturas['iraam']).convert_alpha()
    imagen_iraam = pygame.transform.scale(imagen_iraam, (31, 31))
    # Ira agrupamiento aislamiento
    imagen_iraais = pygame.image.load(texturas['iraais']).convert_alpha()
    imagen_iraais = pygame.transform.scale(imagen_iraais, (31, 31))
    imagen_iraaism = pygame.image.load(texturas['iraaism']).convert_alpha()
    imagen_iraaism = pygame.transform.scale(imagen_iraaism, (31, 31))
    # Ira desplazamiento tipo1
    imagen_iradt1 = pygame.image.load(texturas['iradt1']).convert_alpha()
    imagen_iradt1 = pygame.transform.scale(imagen_iradt1, (31, 31))
    imagen_iradt1m = pygame.image.load(texturas['iradt1m']).convert_alpha()
    imagen_iradt1m = pygame.transform.scale(imagen_iradt1m, (31, 31))
    # Ira desplazamiento tipo2
    imagen_iradt2 = pygame.image.load(texturas['iradt2']).convert_alpha()
    imagen_iradt2 = pygame.transform.scale(imagen_iradt2, (31, 31))
    imagen_iradt2m = pygame.image.load(texturas['iradt2m']).convert_alpha()
    imagen_iradt2m = pygame.transform.scale(imagen_iradt2m, (31, 31))

    # Miedo panico comida
    imagen_miedopc = pygame.image.load(texturas['miedopc']).convert_alpha()
    imagen_miedopc = pygame.transform.scale(imagen_miedopc, (31, 31))
    imagen_miedopcm = pygame.image.load(texturas['miedopcm']).convert_alpha()
    imagen_miedopcm = pygame.transform.scale(imagen_miedopcm, (31, 31))

    # Miedo panico depredador
    imagen_miedopd = pygame.image.load(texturas['miedopd']).convert_alpha()
    imagen_miedopd = pygame.transform.scale(imagen_miedopd, (31, 31))
    imagen_miedopdm = pygame.image.load(texturas['miedopdm']).convert_alpha()
    imagen_miedopdm = pygame.transform.scale(imagen_miedopdm, (31, 31))
    # Miedo agrupamiento
    imagen_miedoa = pygame.image.load(texturas['miedoa']).convert_alpha()
    imagen_miedoa = pygame.transform.scale(imagen_miedoa, (31, 31))
    imagen_miedoam = pygame.image.load(texturas['miedoam']).convert_alpha()
    imagen_miedoam = pygame.transform.scale(imagen_miedoam, (31, 31))
    # Miedo agrupamiento aislamiento
    imagen_miedoais = pygame.image.load(texturas['miedoais']).convert_alpha()
    imagen_miedoais = pygame.transform.scale(imagen_miedoais, (31, 31))
    imagen_miedoaism = pygame.image.load(texturas['miedoaism']).convert_alpha()
    imagen_miedoaism = pygame.transform.scale(imagen_miedoaism, (31, 31))
    # Miedo desplazamiento tipo1
    imagen_miedodt1 = pygame.image.load(texturas['miedodt1']).convert_alpha()
    imagen_miedodt1 = pygame.transform.scale(imagen_miedodt1, (31, 31))
    imagen_miedodt1m = pygame.image.load(texturas['miedodt1m']).convert_alpha()
    imagen_miedodt1m = pygame.transform.scale(imagen_miedodt1m, (31, 31))
    # Miedo desplazamiento tipo2
    imagen_miedodt2 = pygame.image.load(texturas['miedodt2']).convert_alpha()
    imagen_miedodt2 = pygame.transform.scale(imagen_miedodt2, (31, 31))
    imagen_miedodt2m = pygame.image.load(texturas['miedodt2m']).convert_alpha()
    imagen_miedodt2m = pygame.transform.scale(imagen_miedodt2m, (31, 31))

    '''
        Esta función se encarga de cargar todas las listas de botones de los paneles
        con sus respetivos atributos.
    
    def cargar_img_botones(self):
        

        #---------------------- Botones panel opciones ----------------------
        self.lista_btn_opciones.append({'name': "btn_digitar", 'imagen': self.imagen_btn_digitar, 'imagen_pressed': self.imagen_btn_digitar_hoover, 
        'left_top': [852, 211], 'right_bottom': [999, 356],   'mouse_motion': False})

        self.lista_btn_opciones.append({'name': "btn_cargar", 'imagen': self.imagen_btn_cargar, 'imagen_pressed': self.imagen_btn_cargar_hoover, 'left_top': [852, 368],
             'right_bottom': [999, 513],   'mouse_motion': False})


        #---------------------- Botones panel entorno ----------------------

        self.lista_btn_entorno.append({'name': "btn_back", 'imagen': self.imagen_btn_back_entorno, 'imagen_pressed': self.imagen_btn_back_entorno_hoover, 'left_top': [740, 258],
             'right_bottom': [770, 288],   'mouse_motion': False})
        
        self.lista_btn_entorno.append({'name': "btn_next", 'imagen': self.imagen_btn_next_entorno, 'imagen_pressed': self.imagen_btn_next_entorno_hoover, 'left_top': [1041, 457],
             'right_bottom': [1089, 505],   'mouse_motion': False})

    
        #---------------------- Botones panel individuo ----------------------

        self.lista_btn_individuo.append({'name': "btn_back", 'imagen': self.imagen_btn_back_individuo, 'imagen_pressed': self.imagen_btn_back_individuo_hoover, 
        'left_top': [740, 191], 'right_bottom': [770, 221],   'mouse_motion': False})
        
        self.lista_btn_individuo.append({'name': "btn_next", 'imagen': self.imagen_btn_next_individuo, 'imagen_pressed': self.imagen_btn_next_individuo_hoover, 
        'left_top': [1041, 520], 'right_bottom': [1089, 568],   'mouse_motion': False})


        #---------------------- Botones panel alimentación ----------------------

        self.lista_btn_alimentacion.append({'name': "btn_back", 'imagen': self.imagen_btn_back_alimentacion, 'imagen_pressed': self.imagen_btn_back_alimentacion_hoover, 'left_top': [740, 258],
             'right_bottom': [770, 288],   'mouse_motion': False})
        
        self.lista_btn_alimentacion.append({'name': "btn_next", 'imagen': self.imagen_btn_next_alimentacion, 'imagen_pressed': self.imagen_btn_next_alimentacion_hoover, 'left_top': [1041, 457],
             'right_bottom': [1089, 505],   'mouse_motion': False})


        #---------------------- Botones panel depredador ----------------------

        self.lista_btn_depredador.append({'name': "btn_back", 'imagen': self.imagen_btn_back_depredador, 'imagen_pressed': self.imagen_btn_back_depredador_hoover, 
        'left_top': [740, 284], 'right_bottom': [770, 314],   'mouse_motion': False})
        
        self.lista_btn_depredador.append({'name': "btn_next", 'imagen': self.imagen_btn_next_depredador, 'imagen_pressed': self.imagen_btn_next_depredador_hoover, 
        'left_top': [1041, 432], 'right_bottom': [1089, 480], 'on_click': False, 'mouse_motion': False})
    '''    


    """
        Función que permite pintar los botones del panel de control
        
        PARAMETROS
        lista_botones:  Recibe una lista de los botones a pintar, esta lista contiene 
                        las imagenes de los botones y su ubicación en la ventana.
    """
    def dibujar_botones(self, lista_botones):
        
        for boton in lista_botones:
            if(boton['name']=="btn_play"):
                if boton['mouse_motion']:
                    if(self.pause_simulacion):
                        self.ventana.blit(boton['imagen_pressed'], boton['left_top'])
                    if(self.pause_simulacion==False):
                        self.ventana.blit(boton['click_pressed'], boton['left_top']) 
                if(boton['mouse_motion']== False):
                    if(self.pause_simulacion):
                        self.ventana.blit(boton['imagen'], boton['left_top'])
                    if(self.pause_simulacion==False):
                        self.ventana.blit(boton['click'], boton['left_top']) 
            else:
                if boton['mouse_motion']:
                    self.ventana.blit(boton['imagen_pressed'], boton['left_top'])
                else:
                    self.ventana.blit(boton['imagen'], boton['left_top'])

    '''
        Valida si la posición del mouse se encuentra dentro los límites proporcionados del eje X y el eje Y.

        clicX, clicY: Posición del mouse
        posX, posY: limites iniciales del eje x y y.
        posX2, posY2: límites finales del eje x y y.

        RETURN:
            -TRUE:  si el click se encuentra en los límites establecidos
            -FALSE: si el click NO se encuentra en los límites establecidos
    '''
    def validar_clic(self, clicX, clicY, posX, posY, posX2, posY2):
        if(clicX > posX and clicX < posX2 and clicY > posY and clicY < posY2):
            return True
        else:
            return False
    
    '''
        Esta función se encarga de cargar todas las listas de inputs de los paneles
        con sus respetivos atributos.
    '''
    def cargarInputs(self):

        
        # ------------------------ INDIVIDUOS ------------------------------
        self.input_individuos.append(
            {'name': "time_life", 'click':self.imagen_input_able, 
            'off':self.imagen_input_enable, 'check':self.imagen_input_check,
            'left_top': [651+155, 293], 'right_bottom': [1200-221, 651-328], 'edit': True, 'show': False})
        self.input_individuos.append(
            {'name': "reproducciones", 
            'click':self.imagen_input_able, 'off':self.imagen_input_enable, 'check':self.imagen_input_check,
            'left_top': [651+155, 360], 'right_bottom': [1200-211, 651-261], 'edit': False, 'show': False})
        self.input_individuos.append(
            {'name': "tespera", 
             'click':self.imagen_input_able, 'off':self.imagen_input_enable, 'check':self.imagen_input_check,
            'left_top': [651+155,425], 'right_bottom': [1200-221, 651-196], 'edit': False, 'show': False})
        self.input_individuos.append(
            {'name': "talimentacion",
            'click':self.imagen_input_able, 'off':self.imagen_input_enable, 'check':self.imagen_input_check,
            'left_top': [651+155, 490], 'right_bottom': [1200-221, 651-131], 'edit': False, 'show': False})

        
        # ------------------------ DEPREDADOR  ------------------------------
        self.input_depredadores.append(
            {'name': "numero_aparicion", 
            'click':self.imagen_input_able, 'off':self.imagen_input_enable, 'check':self.imagen_input_check,
            'left_top': [651+155, 392], 'right_bottom': [1200-221, 651-229], 'edit': False, 'show': False})
        

        # ------------------------ ALIMENTACION  ------------------------------
        self.input_alimentacion.append(
            {'name': "tasas_aparicion",   
            'click':self.imagen_input_able, 'off':self.imagen_input_enable, 'check':self.imagen_input_check,
            'left_top': [651+155, 360], 'right_bottom': [1200-221, 651-261], 'edit': False, 'show': False})
        self.input_alimentacion.append(
            {'name': "cantidad_recursos", 
            'click':self.imagen_input_able, 'off':self.imagen_input_enable, 'check':self.imagen_input_check,
            'left_top': [651+155, 427], 'right_bottom': [1200-221, 651-194], 'edit': False, 'show': False})


        # ------------------------  ENTORNO  ------------------------------
        self.input_entorno.append(
            {'name': "tamano_poblacion", 
            'click':self.imagen_input_able, 'off':self.imagen_input_enable, 'check':self.imagen_input_check,
            'left_top':[651+153, 359], 'right_bottom': [1200-219, 651-263], 'edit': True, 'ok': False})

        self.input_entorno.append(
            {'name': "prob_explorador", 
            'click':self.imagen_input_able, 'off':self.imagen_input_enable, 'check':self.imagen_input_check,
            'left_top': [651+153, 424], 'right_bottom': [1200-219, 651-196], 'edit': False, 'ok': False})


    '''
        Se encarga de dibujar los diferentes inputs y su contenido
        PARAMETROS 
        - lista_campo: es la lista de inputs que debe mostrar segun el panel
    '''
    def dibujar_input(self, lista_campo):

        for campo in lista_campo:
            if campo['edit']:
                self.ventana.blit(campo['click'], campo['left_top'])
            elif (self.varibales_control[campo['name']] != 0 and campo['edit'] == False):
                self.ventana.blit(campo['check'], campo['left_top'])
            else:
                self.ventana.blit(campo['off'], campo['left_top'])
            
            # Mostramos el contenido del campo si es diferente a 0
            if(self.varibales_control[campo['name']] != 0):
                Fuente = pygame.font.Font(os.path.join(self.fuentes_path,"Roboto-Bold.ttf"), 14)
                titulo = Fuente.render(" " + str(self.varibales_control[campo['name']]), True, (0, 0, 0))
                self.ventana.blit(titulo, (campo['left_top'][0]+7, campo['left_top'][1]+9))
           
        
    '''
        Almacena las propiedades de cada boton en diccionarios y cada boton se añade 
        a la lista del panel que corresponde. Quedando de esta manera una 
        lista de botones (lista de diccionarios) para cada panel
    '''
    def cargar_img_botones(self):
        #---------------------- Botones panel inicio ----------------------
        self.lista_btn_inicio.append({'name': "btn_next_inicio", 'imagen': self.imagen_btn_next_inicio, 'imagen_pressed': self.imagen_btn_next_inicio_hoover, 
        'left_top': [57, 398], 'right_bottom': [145, 437],   'mouse_motion': False})

      
        #---------------------- Botones panel opciones ----------------------
        self.lista_btn_opciones.append({'name': "btn_digitar", 'imagen': self.imagen_btn_digitar, 'imagen_pressed': self.imagen_btn_digitar_hoover, 
        'left_top': [852, 211], 'right_bottom': [999, 356],   'mouse_motion': False})

        self.lista_btn_opciones.append({'name': "btn_cargar", 'imagen': self.imagen_btn_cargar, 'imagen_pressed': self.imagen_btn_cargar_hoover, 'left_top': [852, 368],
             'right_bottom': [999, 513],   'mouse_motion': False})


        #---------------------- Botones panel entorno ----------------------

        self.lista_btn_entorno.append({'name': "btn_back", 'imagen': self.imagen_btn_back_entorno, 'imagen_pressed': self.imagen_btn_back_entorno_hoover, 'left_top': [740, 258],
             'right_bottom': [770, 288],   'mouse_motion': False})
        
        self.lista_btn_entorno.append({'name': "btn_next", 'imagen': self.imagen_btn_next_entorno, 'imagen_pressed': self.imagen_btn_next_entorno_hoover, 'left_top': [1041, 457],
             'right_bottom': [1089, 505],   'mouse_motion': False})

    
        #---------------------- Botones panel individuo ----------------------

        self.lista_btn_individuo.append({'name': "btn_back", 'imagen': self.imagen_btn_back_individuo, 'imagen_pressed': self.imagen_btn_back_individuo_hoover, 
        'left_top': [740, 191], 'right_bottom': [770, 221],   'mouse_motion': False})
        
        self.lista_btn_individuo.append({'name': "btn_next", 'imagen': self.imagen_btn_next_individuo, 'imagen_pressed': self.imagen_btn_next_individuo_hoover, 
        'left_top': [1041, 520], 'right_bottom': [1089, 568],   'mouse_motion': False})


        #---------------------- Botones panel alimentación ----------------------

        self.lista_btn_alimentacion.append({'name': "btn_back", 'imagen': self.imagen_btn_back_alimentacion, 'imagen_pressed': self.imagen_btn_back_alimentacion_hoover, 'left_top': [740, 258],
             'right_bottom': [770, 288],   'mouse_motion': False})
        
        self.lista_btn_alimentacion.append({'name': "btn_next", 'imagen': self.imagen_btn_next_alimentacion, 'imagen_pressed': self.imagen_btn_next_alimentacion_hoover, 'left_top': [1041, 457],
             'right_bottom': [1089, 505],   'mouse_motion': False})


        #---------------------- Botones panel depredador ----------------------

        self.lista_btn_depredador.append({'name': "btn_back", 'imagen': self.imagen_btn_back_depredador, 'imagen_pressed': self.imagen_btn_back_depredador_hoover, 
        'left_top': [740, 284], 'right_bottom': [770, 314],   'mouse_motion': False})
        
        self.lista_btn_depredador.append({'name': "btn_next", 'imagen': self.imagen_btn_next_depredador, 'imagen_pressed': self.imagen_btn_next_depredador_hoover, 
        'left_top': [1041, 432], 'right_bottom': [1089, 480], 'mouse_motion': False})

        
        # ------------------- Botones panel simulación ---------------------------------
        
        self.lista_btn_simulacion.append(
            {'name': "btn_play", 'imagen': self.imagen_btn_play, 'imagen_pressed': self.imagen_btn_play_hoover, 
            'click': self.imagen_btn_pause,'click_pressed': self.imagen_btn_pause_hoover,
            'left_top': [886, 426], 'right_bottom': [966,506], 'mouse_motion': False, 'show': True })
        '''
        self.lista_btn_simulacion.append(
            {'name': "btn_pause", 'imagen': self.imagen_btn_pause_hoover, 'imagen_pressed': self.imagen_btn_pause_hoover, 
            'left_top': [886, 426], 'right_bottom': [966,506], 'mouse_motion': False, 'show': False })
        '''
        self.lista_btn_simulacion.append(
            {'name': "btn_detener", 'imagen': self.imagen_btn_detener, 'imagen_pressed': self.imagen_btn_detener_hoover, 
            'left_top': [651+95, 436], 'right_bottom': [1200-394,651-155], 'mouse_motion': False})
        
        self.lista_btn_simulacion.append(
            {'name': "btn_reiniciar", 'imagen': self.imagen_btn_reiniciar, 'imagen_pressed': self.imagen_btn_reiniciar_hoover, 
            'left_top': [651+395, 436], 'right_bottom': [1200-94,651-155], 'mouse_motion': False})

        '''
        self.lista_btn_simulacion.append(
            {'name': "btn_pause", 'imagen': self.imagen_btn_pause, 'imagen_pressed': self.imagen_btn_pause_hoover, 
            'left_top': [886, 426], 'right_bottom': [966,506], 'mouse_motion': False, 'click': False})
        '''
        self.lista_btn_simulacion.append(
            {'name': "btn_aumentar", 'imagen': self.imagen_btn_aumentar, 'imagen_pressed': self.imagen_btn_aumentar_hoover, 
            'left_top': [651+325, 436], 'right_bottom': [1200-164,651-155], 'mouse_motion': False})
        
        self.lista_btn_simulacion.append(
            {'name': "btn_disminuir", 'imagen': self.imagen_btn_disminuir, 'imagen_pressed': self.imagen_btn_disminuir_hoover, 
            'left_top': [651+165, 436], 'right_bottom': [1200-324,651-155], 'mouse_motion': False})
            

    """ ********** Finaliza la configuracion visual de la simulacion y se da inicio con la configuracion operacional ********** """

    """ Se encarga de construir el array quue simula el mapa
        PARAMETROS:
        -dimension: es el tamaño del array o mapa
    """

    def construirMapa(self, dimension):
        for x in range(dimension):
            self.mapa.append([])
            for y in range(dimension):
                #valor = random.randint(1,3)
                self.mapa[x].append([1, -1])

    '''
    Esta función calcula los porcentajes de incidencia de cada categoria y sub categoria
    que conforma la genética del individuo.

    Returna un diccionario de la forma: 
    categoria, subcategoria_1, subcategoria_2, ...
    {c1,sc11,sc12,c2,c21,c22,c3,c31,c32}
    '''

    def geneticaInicial(self):
        porcentaje_dividido = 100
        # categorias
        panico = random.randint(0, porcentaje_dividido)
        deplazamiento = random.randint(0, (porcentaje_dividido - panico))
        agrupamiento = 100-(deplazamiento + panico)

        # subcategorias
        # panico
        panico_depredador = random.randint(0, porcentaje_dividido)
        panico_comida = porcentaje_dividido - panico_depredador

        # desplazamiento
        tipo_1 = random.randint(0, porcentaje_dividido)
        tipo_2 = porcentaje_dividido - tipo_1

        # agrupamiento
        agruparse = random.randint(0, porcentaje_dividido)
        aislarse = porcentaje_dividido - agruparse

        genetica = {
            'panico': panico,
            'panico_depredador': panico_depredador,
            'panico_comida': panico_comida,
            'desplazamiento': deplazamiento,
            'tipo_1': tipo_1,
            'tipo_2': tipo_2,
            'agrupamiento': agrupamiento,
            'agruparse': agruparse,
            'aislarse': aislarse,
        }

        return genetica

    '''
        Genera una personalidad aleatoria que luego es asignada a un individuo inicial

        RETURN:
        -El array con la personalidad que corresponde al número aleatorio (1 al 5)
    '''
    def personalidadIncicial(self):
        # tipos estructura {key:[positive_value, negative_value, name]}
        tipos = {
            1: [1.5, 0.5, 'Muy Optimista'],
            2: [0.5, 1.5, 'Muy Pesimista'],
            3: [1.2, 0.8, 'Optimista'],
            4: [0.8, 1.2, 'Pesimista'],
            5: [1, 1, 'Neutral'],
        }
        valor = random.randint(1, 5)
        return tipos.get(valor)

    '''Funcion que ubica aleatoriamente en el mapa la cantidad de recursos o zonas de alimentación
      iniciales (la cantidad es tomada del inputtext cantidad_inicial).
    '''

    def zonasAlimentacionInicial(self):
        cantidad_Recursos = self.varibales_control['tamano_grupos']
        for c in range(cantidad_Recursos):
            aux = True
            while aux:
                #El while asegura que la zona es ubicada en un punto libre (donde hay pasto)
                x = random.randint(0, len(self.mapa) - 1)
                y = random.randint(0, len(self.mapa) - 1)
                if(self.mapa[x][y][0] == 1):
                    zonaAlimentacion = {
                        'id': self.idZonaAlimentacion,
                        'pos': [x, y],
                        'capacidad': self.varibales_control['cantidad_recursos'],
                        'pobladores': 0,
                    }
                    self.zonasAlimentacion_array.append(zonaAlimentacion)
                    self.mapa[x][y] = [2, len(self.zonasAlimentacion_array)-1]
                    self.idZonaAlimentacion += 1
                    self.variables_test['zonasAlimentacion'] += 1
                    aux = False
                else:
                    pass
        self.recursosIniciales = False

    """
        Funcion que genera un depredador y lo ubica aleatoriamente en el mapa (el depredador toma un valor de 4 en el mapa)
        El tiempo de aparición de cada depredador es determinado por una variable del panel de control ""
    """

    def crearDepredador(self):
        aux = True
        while aux:
            """ El while asegura que la zona es ubicada en un punto libre (donde hay pasto) """
            x = random.randint(0, len(self.mapa) - 1)
            y = random.randint(0, len(self.mapa) - 1)
            if(self.mapa[x][y][0] == 1):
                depredador = {
                    'pos': [x, y],
                    'vida': self.varibales_control['time_life'] // 4,
                    'tiempo_reposo': 0,
                }
                self.variables_test['depredadores'] += 1
                self.depredadores_array.append(depredador)
                self.mapa[x][y] = [4, len(self.depredadores_array)-1]
                aux = False
            else:
                pass


    """
        Funcion que permite actualizar el arreglo que tiene cada individuo de las zonas de alimentacion
        NOTA: Unicamente se actualiza la informacion de la zona en la cual se encuentra el individuo
    """

    def actualizacionInformacionLocal(self):
        for individuo in self.individuos_array:
            if(individuo['vida'] > 0):
                for zona in individuo['zonas_alimentacion']:
                    if(zona[5]):
                        zonaAlimentacion = self.zonasAlimentacion_array[zona[6]]
                        if(zonaAlimentacion['capacidad'] > 0):
                            zona[2] = zonaAlimentacion['capacidad']
                            zona[3] = zonaAlimentacion['pobladores']
                        else:
                            individuo['zonas_alimentacion'].remove(zona)
                    else:
                        pass

    """
        Funcion que actualiza la informacion de cada zona, tanto su capacidad (que disminuye de acuerdo a la cantidad de individuos)
        como la cantidad de pobladores que se encuentran dos cuadros alrededor
    """

    def actualizacionZonasAlimentacion(self):
        for zona in self.zonasAlimentacion_array:
            if((zona['capacidad']) > 0):
                cantidadPobladores = self.auxCantidadPobladores(zona['pos'], 3)
                zona['pobladores'] = cantidadPobladores
                capacidadTemporal = zona['capacidad'] - cantidadPobladores
                if (capacidadTemporal < 1):
                    zona['capacidad'] = 0
                    self.mapa[zona['pos'][0]][zona['pos'][1]] = [1,-1]
                    self.variables_test['zonasAlimentacion'] -= 1
                else:
                    zona['capacidad'] = capacidadTemporal

    '''
        DESCRIBIR FUNCIÓN
    '''
    def miNuevaZona(self):
        for poblador in self.individuos_array:
            auxCambiarZona = False
            zonasAlimentacion = poblador['zonas_alimentacion']
            for zona in zonasAlimentacion:
                auxCambiarZona = zona[5]
            
            if(not auxCambiarZona):
                responseNuevoDestino = self.nuevoDestino(poblador)
                if (responseNuevoDestino[0]):
                    poblador['negociar'] = False
                    poblador['goHome'] = True
                    poblador['destino'] = [responseNuevoDestino[1], responseNuevoDestino[2]]
                    self.auxSinZonaAlimentacion(poblador)
                    self.actualizarMiZonaAlimentacion(poblador, responseNuevoDestino[1], responseNuevoDestino[2])
                if(len(zonasAlimentacion) == 0):
                    poblador['negociar'] = True
                    poblador['goHome'] = False
                    poblador['destino'] = [random.randint(0, len(self.mapa)-1), random.randint(0, len(self.mapa)-1)]

    
    '''
        DESCRIBIR FUNCIÓN
    '''
    def auxCantidadPobladores(self, posicion, tipo):
        vacias = []
        for row in range(-2, 3):
            for col in range(-2, 3):
                x = posicion[0] + row
                y = posicion[1] + col

                if((x >= 0) and (x < len(self.mapa)) and (y >= 0) and (y < len(self.mapa))):
                        #print("Condiciones: ", (x != poblador['pos_actual'][0]), " ! ", (y != poblador['pos_actual'][1]), " ! ", (x != poblador['pos_anterior'][0]), " ! ", (y != poblador['pos_anterior'][1]) , " ! ", (self.mapa[x][y]==1))
                    if(((x != posicion[0]) or (y != posicion[1])) and (self.mapa[x][y][0] == tipo)):
                        #print("Movimiento ",cont," x = ",x,' y = ',y)
                        vacias.append([x, y])

        for poblador in vacias:
            posArray = self.mapa[poblador[0]][poblador[1]][1]
            individuo = self.individuos_array[posArray]
            if(individuo['vida'] > self.varibales_control['time_life']):
                pass
            else:
                individuo['vida'] = individuo['vida'] + 1
                individuo['talimentacion'] = self.varibales_control['talimentacion']
        return len(vacias)

    """ Funcion que ubica aleatoriamente en el mapa los individuos iniciales (la cantidad es tomada del
        inputtext tamano_poblacion). 
    """

    def ubicacionIndividuosIniciales(self):
        cantidad_Individuos = int(self.varibales_control['tamano_poblacion'])
        dispersion = int(self.varibales_control['dispersion_inicial'])
        for c in range(cantidad_Individuos):
            tipo = 1
            activacionE = round((self.varibales_control['prob_explorador']/100), 2)
            explorador = random.random()
            nacer_explorador = True if explorador <= activacionE else False
            if(nacer_explorador):
                if(self.maximo_exploradores > 0):
                    self.maximo_exploradores -= 1
                    tipo = 2
                else:
                    nacer_explorador = False
            aux = True
            #print("ActivacionE: ", activacionE, " Random: ", explorador, " Tipo Individuo: ", tipo)
            while aux:
                """ El while asegura que el individuo es ubicado en un punto libre (donde hay pasto) """
                ubicacion = [random.randint(
                    0, len(self.mapa) - 1), random.randint(0, len(self.mapa) - 1), 0]
                if(self.mapa[ubicacion[0]][ubicacion[1]][0] == 1 and self.auxDispersion(ubicacion, dispersion)):
                    """ Para que un individuo se pueda ubicar en el mapa, tiene que estar a una distancia 
                        (dispersion) de los demas individuos"""

                    self.matrizdispersa.append(ubicacion)
                    # creo el diccionario del poblador nuevo
                    individuo = {
                        'id': self.idIndividuos,  # El id debe ser unico, todos los individuos deben tener uno
                        'personalidad': self.personalidadIncicial(),
                        # 1: Felicidad 2: Miedo, 3: Ira, 4: Esperanza
                        'emocion': random.randint(1, 4),
                        'pos_actual': [ubicacion[0], ubicacion[1]],
                        'pos_anterior': [ubicacion[0], ubicacion[1]],
                        'destino': [ubicacion[0], ubicacion[1]],
                        #'destino': [random.randint(0, len(self.mapa)-1), random.randint(0, len(self.mapa)-1)],
                        # La estructura de los elmentos debe ser:
                        'zonas_alimentacion': [], 
                        # [posX, #posY, cantidadRecursos, nroIndividuos, idInformante(si se descubrio, el valor es -1), bool que indica si el individuo esta en la zona, idZona]
                        # La estructura de los elmentos debe ser: [posX, #posY, idInformante(si se descubrio, el valor es -1)]
                        'zonas_depredadores': [],
                        'genetica': self.geneticaInicial(),
                        'vida': self.varibales_control['time_life'],
                        #'alimentarse': self.varibales_control['talimentacion'],
                        'me_reproduzco': True,
                        'tribu': 0,
                        'tipo': tipo,  # 1: individuo, 2: explorador, 3: depredador
                        'negociar': True,
                        # por defecto al ser los primeros habitantes
                        'padres': [-1, -1],
                        'mutacion': False,
                        'reputacion': 0,
                        'reputacionNegativa': 0,
                        # "False" Indica si el individuo (explorador) se dirige a reconoer o negociar nuevas ubicaciones.
                        'goHome': False,
                        # "True" si se dirige a su zona para difundir el conocimiento
                        # Atributo que permite aplicar el algoritmo Kmeans al individuo (Y poder agruparlo a una Tribu)
                        'kmeans': True,
                        'reproducciones': self.varibales_control['reproducciones'],
                        'tespera': self.varibales_control['tespera'],
                        'talimentacion': self.varibales_control['talimentacion']
                    }
                    #print('Individuo INFORMACIÓN: ----------- >> ',individuo)
                    # almaceno el individuo en el arreglo de pobladores
                    #self.variables_test['individuos_vivos'] += 1
                    self.individuosVivos += 1
                    self.variables_test['individuosVivos'] += 1
                    self.individuos_array.append(individuo)
                    self.mapa[ubicacion[0]][ubicacion[1]] = [3, individuo['id']]
                    self.idIndividuos += 1
                    self.actualizarDataSet(0, individuo['personalidad'][2], True, individuo['emocion'])
                    aux = False
                else:
                    pass
        self.individuosIniciales = False
       

    """ Funcion auxiliar que compara la distancia de la posible ubicacion respecto a los indiviuos 
        ubicados en el mapa (matriz dispersa), la dispersion se debe cumplir con cada uno de los individos
        ya ubicados.
    """

    def auxDispersion(self, ubicacion, dispersion):
        aux = True
        for individuo in self.matrizdispersa:
            distancia = self.distance(individuo, ubicacion)
            if(distancia < dispersion):
                aux = False
                return aux
            else:
                pass
        return aux

    '''
        DESCRIBIR FUNCIÓN
    '''
    def algoritmoKMeans(self):
        # Arreglo que contiene los centroides para agrupar los individuos, cada centroide
        # tiene posicion x,y
        if(self.individuosVivos > self.varibales_control['tamano_grupos']):
            cluster = []
            clusterBefore = []
            cantidad_cluster = int(self.varibales_control['tamano_grupos'])
            medias = []
            for i in range(cantidad_cluster):
                aux = [0, 0, 1]
                medias.append(aux)
            self.seleccionarCentroides(cluster, medias, cantidad_cluster)

            aux = True
            while aux:
                if(len(clusterBefore) > 0):
                    for i in range(len(cluster)-1):
                        if (cluster[i][0] != clusterBefore[i][0] and cluster[i][1] != clusterBefore[i][1]):
                            aux = True
                            break
                        else:
                            aux = False
                            for i, individuo in enumerate(self.matrizdispersa):
                                if(self.individuos_array[i]['vida'] < 1):
                                    pass
                                else:
                                    if(self.individuos_array[i]['kmeans']):
                                        self.individuos_array[i]['tribu'] = individuo[2]
                                    else:
                                        pass

                for j, ubicacion in enumerate(self.matrizdispersa):
                    distanciaMinima = 1000
                    grupoAsignado = 0

                    for i, centroide in enumerate(cluster):
                        distanciaCluster = self.distance(ubicacion, centroide)
                        if (distanciaCluster < distanciaMinima):
                            distanciaMinima = distanciaCluster
                            grupoAsignado = i
                        else:
                            pass
                    #posIndividuo = self.mapa[ubicacion[0]][ubicacion[1]][1]
                    '''
                    print("j: ", j, " id_individuo: ",self.individuos_array[j]['id'],"  # array: ", len(self.individuos_array), 
                        " # Alimentacion: ",len(self.zonasAlimentacion_array), "Mapa : ", self.mapa[posIndividuo[0]][posIndividuo[1]],
                        "Ubicaciones: ", ubicacion)
                    '''
                    if (self.individuos_array[j]['vida'] < 1):
                        pass
                    else:
                        if(self.individuos_array[j]['kmeans']):
                            self.matrizdispersa[j][2] = (grupoAsignado+1)
                            medias[grupoAsignado][0] = medias[grupoAsignado][0] + \
                                ubicacion[0]
                            medias[grupoAsignado][1] = medias[grupoAsignado][1] + \
                                ubicacion[1]
                            medias[grupoAsignado][2] = medias[grupoAsignado][2] + 1

                clusterBefore = []
                for value in cluster:
                    clusterBefore.append(value)

                cluster = []
                for value in medias:
                    if (value[2] == 0):
                        cluster.append([round(0, 2),  round(0, 2)])
                    else:
                        cluster.append([round(value[0]/value[2], 2),
                                        round(value[1]/value[2], 2)])
        else:
            pass

    '''
        DESCRIBIR FUNCIÓN
    '''
    def seleccionarCentroides(self, cluster, medias, cantidad_cluster):
        if (len(cluster) == 0):
            for c in range(cantidad_cluster):
                aux = True
                while aux:
                    almacenar = True
                    posible = random.randint(0, len(self.matrizdispersa)-1)
                    posX = self.matrizdispersa[posible][0]
                    posY = self.matrizdispersa[posible][1]
                    posArray = self.mapa[posX][posY][1]
                    tipoIndividuo = self.mapa[posX][posY][0]
                    if(tipoIndividuo == 3):
                        if(self.individuos_array[posArray]['vida'] < 1):
                            pass
                        else:
                            if(self.individuos_array[posArray]['kmeans']):
                                for pos in cluster:
                                    if pos[0] == posX and pos[1] == posY:
                                        almacenar = False
                                        break
                                if(almacenar):
                                    cluster.append(
                                        [self.matrizdispersa[posible][0], self.matrizdispersa[posible][1]])
                                    aux = False
                    else:
                        pass
        else:
            pass

    """Función que determina si dos individuos son compatibles para reproducirse o no
        retorna true si suspersonalidades son iguales o opuestas
        de lo contrario false.

        p1 y p2 son arreglos que contienen las personalidad de los individuos

    """
    def individuos_compatibles(self, p1, p2):
        if(((p1[0] == p2[0]) and (p1[1] == p2[1])) or ((p1[0] == p2[1]) and (p1[1] == p2[0]))):
            return True
        else:
            return False

    """Calcula las posiciones que estan vacias 2 cuadros a la redonda de una posición dada
        Retorna la lista de posiciones
    """
    def posiciones_vacias(self, poblador, tipo):
        vacias = []
        for row in range(-2, 3):
            for col in range(-2, 3):
                x = poblador['pos_actual'][0] + row
                y = poblador['pos_actual'][1] + col

                if((x >= 0) and (x < len(self.mapa)) and (y >= 0) and (y < len(self.mapa))):
                        #print("Condiciones: ", (x != poblador['pos_actual'][0]), " ! ", (y != poblador['pos_actual'][1]), " ! ", (x != poblador['pos_anterior'][0]), " ! ", (y != poblador['pos_anterior'][1]) , " ! ", (self.mapa[x][y]==1))
                    if(((x != poblador['pos_actual'][0]) or (y != poblador['pos_actual'][1])) and (self.mapa[x][y][0] == tipo)):
                            #print("Movimiento ",cont," x = ",x,' y = ',y)
                        vacias.append([x, y])
        return vacias

    '''Función que crea el hijo a partir de sus dos padres
    '''
    def reproducir(self, p1, p2):
        activacionE = round((self.varibales_control['prob_explorador']/100), 2)
        explorador = random.random()
        nacer_explorador = True if explorador <= activacionE else False
        if(nacer_explorador):
            if(self.maximo_exploradores > 0):
                self.maximo_exploradores -= 1
            else:
                nacer_explorador = False

        activacionM = round((self.varibales_control['tasa_mutacion']/100), 2)
        mutar = random.random()
        mutacion = True if mutar <= activacionM else False
        #print('activacion = ',activación, '  mutar = ',mutar)
        # categorias
        if(mutacion):
            gen = random.randint(1, 2)
            if (gen == 1):
                mutar = random.randint(1, 3)
                if((mutar == 1) and (p1['genetica']['panico']-6 >= 0) and (p1['genetica']['desplazamiento']+3 <= 100) and (p1['genetica']['agrupamiento']+3 <= 100)):
                    panico = p1['genetica']['panico'] - 6
                    desplazamiento = p1['genetica']['desplazamiento'] + 3
                    agrupamiento = p1['genetica']['agrupamiento'] + 3
                elif((mutar == 2) and (p1['genetica']['panico']+3 <= 100) and (p1['genetica']['desplazamiento']-6 >= 0) and (p1['genetica']['agrupamiento']+3 <= 100)):
                    panico = p1['genetica']['panico'] + 3
                    desplazamiento = p1['genetica']['desplazamiento'] - 6
                    agrupamiento = p1['genetica']['agrupamiento'] + 3
                elif((mutar == 3) and (p1['genetica']['panico']+3 <= 100) and (p1['genetica']['desplazamiento']+3 <= 100) and (p1['genetica']['agrupamiento']-6 >= 0)):
                    panico = p1['genetica']['panico'] + 3
                    desplazamiento = p1['genetica']['desplazamiento'] + 3
                    agrupamiento = p1['genetica']['agrupamiento'] - 6
                else:
                    panico = p1['genetica']['panico']
                    desplazamiento = p1['genetica']['desplazamiento']
                    agrupamiento = p1['genetica']['agrupamiento']
            else:
                mutar = random.randint(1, 3)
                if((mutar == 1) and (p2['genetica']['panico']-6 >= 0) and (p2['genetica']['desplazamiento']+3 <= 100) and (p2['genetica']['agrupamiento']+3 <= 100)):
                    panico = p2['genetica']['panico'] - 6
                    desplazamiento = p2['genetica']['desplazamiento'] + 3
                    agrupamiento = p2['genetica']['agrupamiento'] + 3
                elif((mutar == 2) and (p2['genetica']['panico']+3 <= 100) and (p2['genetica']['desplazamiento']-6 >= 0) and (p2['genetica']['agrupamiento']+3 <= 100)):
                    panico = p2['genetica']['panico'] + 3
                    desplazamiento = p2['genetica']['desplazamiento'] - 6
                    agrupamiento = p2['genetica']['agrupamiento'] + 3
                elif((mutar == 3) and (p2['genetica']['panico']+3 <= 100) and (p2['genetica']['desplazamiento']+3 <= 100) and (p2['genetica']['agrupamiento']-6 >= 0)):
                    panico = p2['genetica']['panico'] + 3
                    desplazamiento = p2['genetica']['desplazamiento'] + 3
                    agrupamiento = p2['genetica']['agrupamiento'] - 6
                else:
                    panico = p2['genetica']['panico']
                    desplazamiento = p2['genetica']['desplazamiento']
                    agrupamiento = p2['genetica']['agrupamiento']
        else:
            gen = random.randint(1, 2)
            if (gen == 1):
                panico = p1['genetica']['panico']
                desplazamiento = p1['genetica']['desplazamiento']
                agrupamiento = p1['genetica']['agrupamiento']
            else:
                panico = p2['genetica']['panico']
                desplazamiento = p2['genetica']['desplazamiento']
                agrupamiento = p2['genetica']['agrupamiento']

        # subcategorias
        # panico
        gen = random.randint(1, 2)
        if (gen == 1):
            panico_depredador = p1['genetica']['panico_depredador']
            panico_comida = p1['genetica']['panico_comida']
        else:
            panico_depredador = p2['genetica']['panico_depredador']
            panico_comida = p2['genetica']['panico_comida']

        # desplazamiento
        gen = random.randint(1, 2)
        if (gen == 1):
            tipo_1 = p1['genetica']['tipo_1']
            tipo_2 = p1['genetica']['tipo_2']
        else:
            tipo_1 = p2['genetica']['tipo_1']
            tipo_2 = p2['genetica']['tipo_2']

        # agrupamiento
        gen = random.randint(1, 2)
        if (gen == 1):
            agruparse = p1['genetica']['agruparse']
            aislarse = p1['genetica']['aislarse']
        else:
            agruparse = p2['genetica']['agruparse']
            aislarse = p2['genetica']['aislarse']

        genetica = {
            'panico': panico,
            'panico_depredador': panico_depredador,
            'panico_comida': panico_comida,
            'desplazamiento': desplazamiento,
            'tipo_1': tipo_1,
            'tipo_2': tipo_2,
            'agrupamiento': agrupamiento,
            'agruparse': agruparse,
            'aislarse': aislarse,
        }

        # seleción de personalidad
        gen = random.randint(1, 2)
        if (gen == 1):
            personalidad = p1['personalidad']
        else:
            personalidad = p2['personalidad']

        # seleccionar posiciones de nacimiento
        ubicaciones = self.posiciones_vacias(
            p1, 1) + self.posiciones_vacias(p2, 1)

        if(ubicaciones):
            pos_nacer = ubicaciones[random.randint(0, len(ubicaciones)-1)]
            #print('NACER----------->', pos_nacer)

            individuo = {
                'id': self.idIndividuos,
                'personalidad': personalidad,
                'emocion': 2,
                'pos_actual': pos_nacer,
                'pos_anterior': pos_nacer,
                'destino': [random.randint(0,len(self.mapa)-1), random.randint(0,len(self.mapa)-1)],
                'zonas_alimentacion': p1['zonas_alimentacion'], 
                'zonas_depredadores': p1['zonas_depredadores'], 
                'genetica': genetica,
                'vida': self.varibales_control['time_life'],
                #'alimentarse': 2,
                'me_reproduzco': True,
                'tribu': p1['tribu'],
                'tipo': 2 if nacer_explorador == True else 1,  # 1: individuo, 2: explorador,
                'negociar': True,
                'padres': [p1['id'], p2['id']],
                'mutacion': mutacion,
                'goHome': False,
                'reputacion': 0,
                'reputacionNegativa': 0,
                'kmeans': True,
                'reproducciones': self.varibales_control['reproducciones'],
                'tespera': self.varibales_control['tespera'], 
                'talimentacion': self.varibales_control['talimentacion'],
            }

            # se añaden las zonas de alimentación del segundo padre, aquí se verifica que no hayan repetidas
            self.añadirZonas(p2['zonas_alimentacion'],individuo['zonas_alimentacion'])
            # se añaden las zonas de depredadores del segundo padre, aquí se verifica que no hayan repetidas
            self.añadirZonasDepredador(p2['zonas_depredadores'],individuo['zonas_depredadores'])

            #print('NACER----------->', pos_nacer,
                  #'  tipo = ', individuo['tipo'])
            # añado el individio al array de pobladores global
            self.hijos_array.append(individuo)
            #print("naci")
            #print(self.hijos_array)

            # indico en la matriz mapa que debe haber un individuo e x,y posicion, temporalmente se
            # ubica el numero 5 para dan a entender que la casilla esta ocupada. Posteriormente se debe 
            # actualizar este valor por 3
            self.mapa[pos_nacer[0]][pos_nacer[1]] = [5, individuo['id']]
            # Se actualiza la matriz dispersa para poder agrupar correctamente los nuevos individuos
            self.matrizdispersa.append([individuo['pos_actual'][0], individuo['pos_actual'][1], 0])
            # marco como reproducidos a los padres para que solo se reproduzcan una vez por generación
            p1['me_reproduzco'] = False
            p2['me_reproduzco'] = False
            p1['reproducciones'] -= 1
            p2['reproducciones'] -= 1

            #Aumento el contador de id para el proximo individuo y garantizar que sean únicos
            self.idIndividuos += 1
            self.individuosVivos += 1
            self.variables_test['individuosVivos'] += 1
            self.actualizarDataSet(0, individuo['personalidad'][2], True, individuo['emocion'])

    ''' Reproducción de individuos '''
    def reproduccionIndividuos(self):
        for poblador in self.individuos_array:
            # distancia_individuos = 0 Nunca se utiliza
            #print("---------------- POBLADOR ------>  ", poblador)
            if(poblador['vida'] > 0):
                if(poblador['reproducciones'] > 0):
                    if(poblador['me_reproduzco']):
                        posibles_parejas = self.parejasIndividuo(poblador)
                        if(posibles_parejas):
                            pareja = random.randint(0, len(posibles_parejas)-1)
                            self.reproducir(poblador, self.individuos_array[posibles_parejas[pareja]])
                    else:
                        poblador['tespera'] -= 1
                        if(poblador['tespera'] < 0):
                            poblador['me_reproduzco'] = True
                            poblador['tespera'] = self.varibales_control['tespera']   
                        #print('reproducción p1 =', poblador['me_reproduzco'], 'p2 = ', pobladores[pareja]['me_reproduzco'])
                else:
                    #print("Ya no tengo reproducciones")
                    pass
            else:
                #print("No me puedo reproducir, estoy muerto")
                pass
                

    ''' esta función setea el atributo me_reproduzco de cada individuo a true, se llama después de la 
    reproducción, para poder habilitar quienes puedan reproducirse en la siguiente generación
    '''
    def actualizarMeReproduzco(self):
        for p in self.individuos_array:
            if(p['vida'] > 0):
                if(p['tespera'] == 0 and p['reproducciones']>0):
                    #print("Ya me puedo reproducir: ", p['id'])
                    p['me_reproduzco'] = True
                    p['tespera'] = self.varibales_control['tespera'] 

    '''
        Funcion que devuelve un array  de los id de las parejas con las cuales se puede reproducir el pobaldor.
    '''
    def parejasIndividuo(self, poblador):
        posibles_parejas = []
        for row in range(-2, 3):
            for col in range(-2, 3):
                x = poblador['pos_actual'][0] + row
                y = poblador['pos_actual'][1] + col

                if((x >= 0) and (x < len(self.mapa)) and (y >= 0) and (y < len(self.mapa))):
                    #print("Condiciones: ", (x != poblador['pos_actual'][0]), " ! ", (y != poblador['pos_actual'][1]), " ! ", (x != poblador['pos_anterior'][0]), " ! ", (y != poblador['pos_anterior'][1]) , " ! ", (self.mapa[x][y]==1))
                    if(((x != poblador['pos_actual'][0]) or (y != poblador['pos_actual'][1])) and (self.mapa[x][y][0] == 3)):
                        #print("Ubicacion en el mapa: ", x, " - ", y, " tupla del mapa: ", self.mapa[x][y])
                        if ((self.individuos_array[self.mapa[x][y][1]]['vida'] > 0) and (self.individuos_array[self.mapa[x][y][1]]['me_reproduzco']) and (self.individuos_array[self.mapa[x][y][1]]['reproducciones']>0)):
                            if ((self.individuos_compatibles(poblador['personalidad'], self.individuos_array[self.mapa[x][y][1]]['personalidad']))):
                                # guardo el id que ocupa el individuo en el array individuos_array accediendo al array que contiene el mapa
                                #print("C O N D I C I C I O N ---> compatibles")
                                posibles_parejas.append(self.mapa[x][y][1])
                        else:
                            #print("No puedo ser pareja, estoy muerto")
                            pass
        return posibles_parejas

    '''Se pinta el mapa en pantalla'''
    def dibujarMapa(self, dimension):
        # posiciones en pixeles para poder ubicar la imagen
        posx = 0
        posy = 0
        for x in range(dimension):
            for y in range(dimension):
                if(self.mapa[x][y][0] == 1):
                    self.ventana.blit(self.imagen_grass, (posx, posy))
                elif(self.mapa[x][y][0] == 2):
                    self.ventana.blit(self.imagen_water, (posx, posy))
                elif(self.mapa[x][y][0] == 4):
                    self.ventana.blit(self.imagen_predator, (posx, posy))
                else:
                    # pinto en el tablero el poblador
                    #self.ventana.blit(self.imagen_felizaism, (posx,posy))
                    self.pintarIndividuo(self.mapa[x][y][1], posx, posy)
                posx += 31
            posy += 31
            posx = 0
    '''Función que pinta los individuos de acuerdo a su tendencia de comportamiento y emoción'''

    '''
        DESCRIBIR FUNCIÓN
    '''
    def pintarIndividuo(self, id, posx, posy):
        if(self.individuos_array[id]['tipo'] == 2):
            self.ventana.blit(self.imagen_explorador, (posx, posy))
        else:

            if(self.individuos_array[id]['emocion'] == 1):
                if((self.individuos_array[id]['genetica']['panico'] >= self.individuos_array[id]['genetica']['desplazamiento']) and
                        (self.individuos_array[id]['genetica']['panico'] >= self.individuos_array[id]['genetica']['agrupamiento'])):

                    if(self.individuos_array[id]['genetica']['panico_comida'] >= self.individuos_array[id]['genetica']['panico_depredador']):
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_felizpcm, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_felizpc, (posx, posy))
                    else:
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_felizpdm, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_felizpd, (posx, posy))

                elif((self.individuos_array[id]['genetica']['desplazamiento'] >= self.individuos_array[id]['genetica']['panico']) and
                     (self.individuos_array[id]['genetica']['desplazamiento'] >= self.individuos_array[id]['genetica']['agrupamiento'])):

                    if(self.individuos_array[id]['genetica']['tipo_1'] >= self.individuos_array[id]['genetica']['tipo_2']):
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_felizdt1m, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_felizdt1, (posx, posy))
                    else:
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_felizdt2m, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_felizdt2, (posx, posy))
                else:
                    if(self.individuos_array[id]['genetica']['agruparse'] >= self.individuos_array[id]['genetica']['aislarse']):
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_felizam, (posx, posy))
                        else:
                            self.ventana.blit(self.imagen_feliza, (posx, posy))
                    else:
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_felizaism, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_felizais, (posx, posy))

            elif(self.individuos_array[id]['emocion'] == 2):
                if((self.individuos_array[id]['genetica']['panico'] >= self.individuos_array[id]['genetica']['desplazamiento']) and
                        (self.individuos_array[id]['genetica']['panico'] >= self.individuos_array[id]['genetica']['agrupamiento'])):

                    if(self.individuos_array[id]['genetica']['panico_comida'] >= self.individuos_array[id]['genetica']['panico_depredador']):
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(self.imagen_irapcm, (posx, posy))
                        else:
                            self.ventana.blit(self.imagen_irapc, (posx, posy))
                    else:
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(self.imagen_irapdm, (posx, posy))
                        else:
                            self.ventana.blit(self.imagen_irapd, (posx, posy))

                elif((self.individuos_array[id]['genetica']['desplazamiento'] >= self.individuos_array[id]['genetica']['panico']) and
                     (self.individuos_array[id]['genetica']['desplazamiento'] >= self.individuos_array[id]['genetica']['agrupamiento'])):

                    if(self.individuos_array[id]['genetica']['tipo_1'] >= self.individuos_array[id]['genetica']['tipo_2']):
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_iradt1m, (posx, posy))
                        else:
                            self.ventana.blit(self.imagen_iradt1, (posx, posy))
                    else:
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_iradt2m, (posx, posy))
                        else:
                            self.ventana.blit(self.imagen_iradt2, (posx, posy))
                else:
                    if(self.individuos_array[id]['genetica']['agruparse'] >= self.individuos_array[id]['genetica']['aislarse']):
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(self.imagen_iraam, (posx, posy))
                        else:
                            self.ventana.blit(self.imagen_iraa, (posx, posy))
                    else:
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_iraaism, (posx, posy))
                        else:
                            self.ventana.blit(self.imagen_iraais, (posx, posy))

            elif (self.individuos_array[id]['emocion'] == 3):
                if((self.individuos_array[id]['genetica']['panico'] >= self.individuos_array[id]['genetica']['desplazamiento']) and
                        (self.individuos_array[id]['genetica']['panico'] >= self.individuos_array[id]['genetica']['agrupamiento'])):

                    if(self.individuos_array[id]['genetica']['panico_comida'] >= self.individuos_array[id]['genetica']['panico_depredador']):
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_esperanzapcm, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_esperanzapc, (posx, posy))
                    else:
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_esperanzapdm, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_esperanzapd, (posx, posy))

                elif((self.individuos_array[id]['genetica']['desplazamiento'] >= self.individuos_array[id]['genetica']['panico']) and
                     (self.individuos_array[id]['genetica']['desplazamiento'] >= self.individuos_array[id]['genetica']['agrupamiento'])):

                    if(self.individuos_array[id]['genetica']['tipo_1'] >= self.individuos_array[id]['genetica']['tipo_2']):
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_esperanzadt1m, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_esperanzadt1, (posx, posy))
                    else:
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_esperanzadt2m, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_esperanzadt2, (posx, posy))
                else:
                    if(self.individuos_array[id]['genetica']['agruparse'] >= self.individuos_array[id]['genetica']['aislarse']):
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_esperanzaam, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_esperanzaa, (posx, posy))
                    else:
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_esperanzaaism, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_esperanzaais, (posx, posy))
            else:
                if((self.individuos_array[id]['genetica']['panico'] >= self.individuos_array[id]['genetica']['desplazamiento']) and
                        (self.individuos_array[id]['genetica']['panico'] >= self.individuos_array[id]['genetica']['agrupamiento'])):

                    if(self.individuos_array[id]['genetica']['panico_comida'] >= self.individuos_array[id]['genetica']['panico_depredador']):
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_miedopcm, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_miedopc, (posx, posy))
                    else:
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_miedopdm, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_miedopd, (posx, posy))

                elif((self.individuos_array[id]['genetica']['desplazamiento'] >= self.individuos_array[id]['genetica']['panico']) and
                     (self.individuos_array[id]['genetica']['desplazamiento'] >= self.individuos_array[id]['genetica']['agrupamiento'])):

                    if(self.individuos_array[id]['genetica']['tipo_1'] >= self.individuos_array[id]['genetica']['tipo_2']):
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_miedodt1m, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_miedodt1, (posx, posy))
                    else:
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_miedodt2m, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_miedodt2, (posx, posy))
                else:
                    if(self.individuos_array[id]['genetica']['agruparse'] >= self.individuos_array[id]['genetica']['aislarse']):
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_miedoam, (posx, posy))
                        else:
                            self.ventana.blit(self.imagen_miedoa, (posx, posy))
                    else:
                        if(self.individuos_array[id]['mutacion']):
                            self.ventana.blit(
                                self.imagen_miedoaism, (posx, posy))
                        else:
                            self.ventana.blit(
                                self.imagen_miedoais, (posx, posy))

    ''' Calcula la distancia (pitágoras) entre dos puntos celdas'''
    def distance(self, p0, p1):
        return math.sqrt((p0[0] - p1[0])**2 + (p0[1] - p1[1])**2)

    ''' Indica si un individuo se encuentra a 2 cuadros a la redonda de una posición '''
    def llegueAlDestino(self, destino, actual):
        if((destino[0] == actual[0]) and (destino[1] == actual[1])): #LLegó al destino
            return True
        
        x_destino = self.distance(destino, actual)

        if(x_destino <= 2.9): #Quiere decir que está a 2 casillas del destino
            if(self.mapa[destino[0]][destino[1]][0] == 1):
                return False #Puedo llegar
            if(x_destino >= 1.42): #Identificamos si esta exactament a una casilla
                
                #Revisamos si hay casillas dispoibles a 1 casilla del destino, Con al menos una se puede retornar que si puede avanzar
                #De esta manera evitaríamos hacer todo el recorrido del for y optimizar un poco
                for row in range(-1, 2):
                    for col in range(-1, 2):
                        x = actual[0] + row
                        y = actual[1] + col

                        if((x >= 0) and (x < len(self.mapa)) and (y >= 0) and (y < len(self.mapa))):
                            if(((x != actual[0]) or (y != actual[1])) and (self.mapa[x][y][0] == 1)):
                                return False  
                return True
                
            else:
                return True # esta a 1 casilla y ya no puede avanzar al destino
        else:
            return False # Estoy a más de 2 casillas
    
    ''' Desplazamiento de los individuos por generación '''
    def moverIndividuos(self, pobladores):
        for i, poblador in enumerate(pobladores):
            x = 0
            y = 0
            celdas_vecinas = {}
            movimientos = {}
            #print('Posicion actual poblador x = ', poblador['pos_actual'][0], 'poblador y = ', poblador['pos_actual'][1])
            cont = 1

            if (poblador['vida'] > 0):
                if(self.llegueAlDestino(poblador['destino'],poblador['pos_actual'])):
                    poblador['destino'][0] = poblador['pos_actual'][0]
                    poblador['destino'][1] = poblador['pos_actual'][1]
                    self.reputacionNegativa(poblador['destino'], poblador)
                    if(poblador['tipo'] == 2):
                        if(poblador['goHome']):
                            poblador['kmeans'] = True
                            poblador['goHome'] = False
                            self.algoritmoKMeans()
                            self.difundirInformacion(poblador)
                            poblador['destino'] = [random.randint(0, len(self.mapa)-1),random.randint(0, len(self.mapa)-1)]
                            poblador['negociar'] = True
                            poblador['kmeans'] = False
                        else:
                            poblador['goHome'] = True
                            poblador['kmeans'] = False
                            poblador['negociar'] = False
                            poblador['destino'] = self.posMiZonaAlimentacion(poblador)

                    else:
                        poblador['goHome'] = False
                        poblador['kmeans'] = True
                else:
                    for row in range(-1, 2):
                        for col in range(-1, 2):
                            x = poblador['pos_actual'][0] + row
                            y = poblador['pos_actual'][1] + col
                            #print("Posible x: ", x, " y: ", y, 'matriz: ', len(self.mapa))

                            #print("Condiciones: ", x>=0, " ! ",x<len(self.mapa), " ! ", y>=0 , " ! ", y<len(self.mapa), " ! ")
                            if((x >= 0) and (x < len(self.mapa)) and (y >= 0) and (y < len(self.mapa))):
                                #print("Condiciones: ", (x != poblador['pos_actual'][0]), " ! ", (y != poblador['pos_actual'][1]), " ! ", (x != poblador['pos_anterior'][0]), " ! ", (y != poblador['pos_anterior'][1]) , " ! ", (self.mapa[x][y]==1))
                                if((((x != poblador['pos_actual'][0]) or (y != poblador['pos_actual'][1])) and
                                        ((x != poblador['pos_anterior'][0]) or (y != poblador['pos_anterior'][1]))) and (self.mapa[x][y][0] == 1)):
                                    #print("Movimiento ",cont," x = ",x,' y = ',y)
                                    celdas_vecinas.setdefault(cont, [x, y])
                                    movimientos.setdefault(cont, self.distance([x, y], poblador['destino']))
                                    cont += 1
                                    #print('contador ---> ',cont)
                                else:
                                    pass
                                    #print("Se descarta", (self.mapa[x][y][0]==1))
                            else:
                                pass
                                # print("Over")
                    #print('VECINAS =', celdas_vecinas, 'DISTANCIAS =', movimientos)

                    # ordeno los movimientos de menor a mayor distancia sale en forma de lista
                    movimientos_ordenados = sorted(movimientos.items(), key=operator.itemgetter(1))
                    #print("Movimientos ordenados: ", movimientos_ordenados)
                    for mover in movimientos_ordenados:
                        #pregunta si es pasto
                        if (self.mapa[celdas_vecinas[mover[0]][0]] [celdas_vecinas[mover[0]][1]][0] == 1):
                            #asigna la posición del individuo en el array
                            posicionArrayIndividuos = self.mapa[poblador['pos_actual'][0]][poblador['pos_actual'][1]][1] 
                            # indico que debe ir pasto al lugar libre que va a dejar el poblador
                            self.mapa[poblador['pos_actual'][0]][poblador['pos_actual'][1]] = [1, -1]

                            
                            # actualizo la posición anterior con la actual
                            poblador['pos_anterior'][0] = poblador['pos_actual'][0]
                            poblador['pos_anterior'][1] = poblador['pos_actual'][1]

                            # Actualizamos la posición del individuo en la matriz dispersa que
                            # que se utiliza para el agrupamiento de los individuos
                            self.matrizdispersa[i][0] = celdas_vecinas[mover[0]][0]
                            self.matrizdispersa[i][1] = celdas_vecinas[mover[0]][1]

                            # actualizo la posición actual con el movimiento elegido
                            poblador['pos_actual'][0] = celdas_vecinas[mover[0]][0]
                            poblador['pos_actual'][1] = celdas_vecinas[mover[0]][1]

                            #print('anterior modificada:(',poblador['pos_anterior'][0],' ,',poblador['pos_anterior'][1],')')
                            #print('actual modificada x = ', poblador['pos_actual'][0], ' y= ', poblador['pos_actual'][1])

                            # indico en la matriz que debe ir un poblador
                            self.mapa[celdas_vecinas[mover[0]][0]][celdas_vecinas[mover[0]][1]] = [3, poblador['id']]
                            break

    '''
        DESCRIBIR FUNCIÓN
    '''
    def negociacion(self):
        for indice, individuo in enumerate(self.individuos_array):
            if(individuo['vida'] > 0):
                if(individuo['tipo'] == 2):
                    if(not individuo['negociar']):
                        pass
                    else:
                        posIndivudosCercanos = self.individuosCercanos(
                            individuo['pos_actual'])
                        #self.mostrarIndividuos(individuo, posIndivudosCercanos, indice)
                        for negociador in posIndivudosCercanos:
                            if(self.individuos_array[negociador]['negociar']):
                                if(self.individuos_array[negociador]['tribu'] == individuo['tribu']):
                                    pass
                                else:
                                    #self.mostrarIndividuos(self.individuos_array[negociador], ['Negociador'], negociador)
                                    self.teoriaJuegos(indice, negociador)
                            else:
                                pass
                else:
                    pass

    """
        Funcion que observa alrededor y almacena la posicion de los individuos que están cerca
    """
    def individuosCercanos(self, pos):
        vacias = []
        for row in range(-1, 2):
            for col in range(-1, 2):
                x = pos[0] + row
                y = pos[1] + col

                if((x >= 0) and (x < len(self.mapa)) and (y >= 0) and (y < len(self.mapa))):
                    if(((x != pos[0]) or (y != pos[1])) and (self.mapa[x][y][0] == 3)):
                        vacias.append(self.mapa[x][y][1])
        return vacias

    """
        Funcion que observa 2 cuadros alrededor y almacena el depredador y luego retorna los depredadores cerca
    """
    def depredadoresCercanos(self, pos):
        depredadores = []
        for row in range(-2, 3):
            for col in range(-2, 3):
                x = pos[0] + row
                y = pos[1] + col

                if((x >= 0) and (x < len(self.mapa)) and (y >= 0) and (y < len(self.mapa))):
                    if(((x != pos[0]) or (y != pos[1])) and (self.mapa[x][y][0] == 4)):
                        depredador = self.depredadores_array[self.mapa[x][y][1]]
                        depredadores.append(
                            [depredador['pos'][0], depredador['pos'][1], -1])
        return depredadores

    """
        Funcion que observa 2 cuadros alrededor y almacena la zona alimentación y luego retorna las zonas cerca
    """
    def alimentacionCercana(self, pos):
        alimentacion = []
        for row in range(-2, 3):
            for col in range(-2, 3):
                x = pos[0] + row
                y = pos[1] + col

                if((x >= 0) and (x < len(self.mapa)) and (y >= 0) and (y < len(self.mapa))):
                    if(((x != pos[0]) or (y != pos[1])) and (self.mapa[x][y][0] == 2)):
                        alimentacion.append([self.zonasAlimentacion_array[self.mapa[x][y][1]]['pos'][0],
                                             self.zonasAlimentacion_array[self.mapa[x]
                                                                          [y][1]]['pos'][1],
                                             self.zonasAlimentacion_array[self.mapa[x]
                                                                          [y][1]]['capacidad'],
                                             self.zonasAlimentacion_array[self.mapa[x]
                                                                          [y][1]]['pobladores'],
                                             -1,
                                             False,
                                             self.zonasAlimentacion_array[self.mapa[x][y][1]]['id']])
        #print('ALIMENTACION --->', alimentacion)
        return alimentacion
    
    '''
    Calcula el máximo de exploradores pora una generación, descontando los que ya existen.
    '''
    def calcularMaximoExploradores(self):
        exploradores_actuales= 0
        if(self.individuos_array):
            for p in self.individuos_array: #cuento los exploradores actuales
                if((p['tipo'] == 2) and (p['vida'] > 0)):
                    exploradores_actuales += 1
            self.maximo_exploradores = ((len(self.individuos_array)-1) * (self.varibales_control['prob_explorador']/100)) - exploradores_actuales

    ''' funcion que le permite al individuo mirar las zonasde alimentacion cercanas a su posición
    '''
    def zonasCercanasAlimentacion(self, pobladores):
        for poblador in pobladores:
            #print("Zonas cercanas: ", poblador["id"])
            if(poblador['vida'] > 0):             

                zonas = []
                zonas = self.alimentacionCercana(poblador['pos_actual'])
                responseAñadirZonas = self.añadirZonas(zonas, poblador['zonas_alimentacion'])
                #print('Zonas poblador --->', poblador['zonas_alimentacion'])
                #print(not zonas)
                # se cuestiona si cambia de destino o no
                if(responseAñadirZonas):
                    if(poblador['goHome']):
                        pass
                    else:
                        responseNuevoDestino = self.nuevoDestino(poblador)
                        if (responseNuevoDestino[0]):
                            poblador['negociar'] = False
                            poblador['goHome'] = True
                            poblador['destino'] = [responseNuevoDestino[1], responseNuevoDestino[2]]
                            self.auxSinZonaAlimentacion(poblador)
                            self.actualizarMiZonaAlimentacion(poblador, responseNuevoDestino[1], responseNuevoDestino[2])
                        else:
                            poblador['negociar'] = False
                            poblador['goHome'] = True
                            poblador['destino'] = self.posMiZonaAlimentacion(poblador)
                else:
                    pass
                
                
    ''' funcion que le permite al individuo mirar las zonasde depredadores cercanas a su posición
    '''
    def zonasCercanasDepredadores(self, pobladores):
        for poblador in pobladores:
            #print("Zonas cercanas: ", poblador["id"])
            if(poblador['vida'] > 0):
                # zonas depredadores
                nuevosDepredadores = self.depredadoresCercanos(poblador['pos_actual'])
                depredadoresActuales = poblador['zonas_depredadores']
                
                if(len(depredadoresActuales)>0):
                    for element in nuevosDepredadores:
                        if (element in depredadoresActuales):
                            depredadoresActuales.remove(element)
                poblador['zonas_depredadores'] = nuevosDepredadores + depredadoresActuales         

    '''
        Entre dos listas añade un elemento a la lista de zonas del poblador solo si es diferente,
        si ya se encuentra verifica que sea de otro informate y actualiza la reputación,
        ademas tiene en cuenta si hay variacion en la capacidad y pobladores para actualizar los valores
    '''

    def añadirZonas(self, zonas, zonas_poblador):
        agregarZona = [0,0,0,0,0,0,0]
        add = False
        if(zonas):
            add = True
            if(zonas_poblador):
                for zona_nueva in zonas:
                    for zona_vieja in zonas_poblador:
                        # x,y iguales
                        if((zona_nueva[0] == zona_vieja[0])and(zona_nueva[1] == zona_vieja[1])):
                            add = False
                            #print(zona_vieja)# para verificar el ERROR DEL DEPREDADOR
                            if(zona_vieja[4] != -1):  # informante diferente
                                self.individuos_array[zona_vieja[4]]['reputacion'] += 1
                                zona_vieja[4] = -1
                                # capacidades o pobladores diferentes
                                if((zona_nueva[2] != zona_vieja[2]) or (zona_nueva[3] != zona_vieja[3])):
                                    zona_vieja[2] = zona_nueva[2]
                                    zona_vieja[3] = zona_nueva[3]
                            # capacidades o pobladores diferentes
                            elif((zona_nueva[2] != zona_vieja[2]) or (zona_nueva[3] != zona_vieja[3])):
                                zona_vieja[2] = zona_nueva[2]
                                zona_vieja[3] = zona_nueva[3]
                    if(add):
                        for x in range(7):
                            agregarZona[x] = zona_nueva[x]
                        agregarZona[5] = False
                        zonas_poblador.append(agregarZona)

            else:
                zonas_poblador += zonas
                for i, element in enumerate(zonas_poblador):
                    if(i > 0):
                        element[5] = False
        return add
                

    '''
        DESCRIBIR FUNCIÓN
    '''
    def añadirZonasDepredador(self, zonas, zonas_poblador):
        add = True
        if(zonas):
            if(zonas_poblador):
                for zona_nueva in zonas:
                    for zona_vieja in zonas_poblador:
                        # x,y iguales
                        if((zona_nueva[0] == zona_vieja[0])and(zona_nueva[1] == zona_vieja[1])):
                            add = False
                            if(zona_vieja[2] != -1):  # informante diferente
                                self.individuos_array[zona_vieja[2]]['reputacion'] += 1
                                zona_vieja[2] = -1

                    if(add):
                        zonas_poblador.append(zona_nueva)

            else:
                zonas_poblador += zonas

    """ 
       Funcion que recibe los indices del explorador y poblador que tendran el intercambio de informacion
       Para determinar si el individuo traicionará o no, se tiene en cuenta el arreglo de zonas de alimentacion 
       de cada uno y la emocion que está predominando en el momento. Con estos datos se invoca 
       la funcion coopearTraicionar
    """

    def teoriaJuegos(self, explorador, negociador):
        zonasAlimentacionExplorador = self.individuos_array[explorador]['zonas_alimentacion']
        emocionExplorador = self.individuos_array[explorador]['emocion']
        reputacionExplorador = [self.individuos_array[explorador]['reputacion'], self.individuos_array[explorador]['reputacionNegativa']]
        personalidadExplorador = self.individuos_array[explorador]['personalidad']

        zonasAlimentacionNegociador = self.individuos_array[negociador]['zonas_alimentacion']
        emocionNegociador = self.individuos_array[negociador]['emocion']
        reputacionNegociador = [self.individuos_array[negociador]['reputacion'], self.individuos_array[negociador]['reputacionNegativa']]
        personalidadNegociador = self.individuos_array[negociador]['personalidad']

        decisionExplorador = self.cooperarTraicionar(emocionExplorador, zonasAlimentacionExplorador, zonasAlimentacionNegociador, reputacionNegociador, personalidadExplorador)
        decisionNegociador = self.cooperarTraicionar(emocionNegociador, zonasAlimentacionNegociador, zonasAlimentacionExplorador, reputacionExplorador, personalidadNegociador)
        #print("Explorador decidio: ", decisionExplorador,
         #     " | Negociador decidio: ", decisionNegociador)

        self.compartirInformacion([explorador, decisionExplorador], [
                                  negociador, decisionNegociador])

        # Los valors de negociar son actualizados para que la negociación se limite a un vez por generacion (iteracion)
        responseNuevoDestino = self.nuevoDestino(self.individuos_array[explorador])

        if (responseNuevoDestino[0]):
            self.individuos_array[explorador]['negociar'] = False
            self.individuos_array[explorador]['goHome'] = True
            self.individuos_array[explorador]['destino'] = [responseNuevoDestino[1], responseNuevoDestino[2]]
            self.auxSinZonaAlimentacion(self.individuos_array[explorador])
            self.actualizarMiZonaAlimentacion(self.individuos_array[explorador], responseNuevoDestino[1], responseNuevoDestino[2])
        else:
            self.individuos_array[explorador]['negociar'] = False
            self.individuos_array[explorador]['goHome'] = True
            self.individuos_array[explorador]['destino'] = self.posMiZonaAlimentacion(self.individuos_array[explorador])
        
        self.difundirInformacion(self.individuos_array[negociador])
        responseNuevoDestino = self.nuevoDestino(self.individuos_array[negociador])
        if (responseNuevoDestino[0]):
            self.individuos_array[negociador]['negociar'] = False
            self.individuos_array[negociador]['goHome'] = True
            self.individuos_array[negociador]['destino'] = [responseNuevoDestino[1], responseNuevoDestino[2]]
            self.auxSinZonaAlimentacion(self.individuos_array[negociador])
            self.actualizarMiZonaAlimentacion(self.individuos_array[negociador], responseNuevoDestino[1], responseNuevoDestino[2])
        else:
            self.individuos_array[negociador]['negociar'] = True

    """
        Se toman los valores de la capacidadZona de alimentación, cantidad de individuos de la zona en la cual se encuentra el individuos
        De acuerdo a la emocion se da prioridad a alguna de estas variables o en ocasiones a las dos para determinar si se coopera o 
            se traiciona
    """

    def cooperarTraicionar(self, emocion, zonasAlimentacionPropias, zonasAlimentacionNegociante, reputacionNegociante, personalidad):
        capacidadZona = 0
        cantidadIndividuos = 0
        cooperar = False

        activacionPositiva = round((reputacionNegociante[0]/10), 2)
        randomPositivo = random.random()

        activacionNegativa = round((reputacionNegociante[1]/10), 2)
        randomNegativo = random.random()

        reputacionPositiva = True if randomPositivo <= activacionPositiva else False #Si el valor random esta en el intervalo de la activacion, 
        #el individuo decide cooperar dado que el negociante tiene una buena reputacion

        reputacionNegativa = False if randomNegativo <= activacionNegativa else True #Si el valor random esta en el intervalo de la activacion, 
        #el individuo decide no cooperar (False) dado que el negociante tiene una mala reputacion

        for zona in zonasAlimentacionPropias:
            if(zona[5]):
                capacidadZona = zona[2]
                cantidadIndividuos = zona[3]
            else:
                pass
        if(emocion == 1):  # Al individuo lo domina la emocion de Felicidad: Busca una ubicacion con mayor capacidad y mayor cantidad individuos
            for zona in zonasAlimentacionNegociante:
                if (zona[2] > capacidadZona and zona[3] > cantidadIndividuos):
                    cooperar = reputacionNegativa
                else:
                    cooperar = reputacionPositiva
        if(emocion == 2):  # Al individuo lo domina la emocion de Ira: Busca ubicaciones con más pobladores
            for zona in zonasAlimentacionNegociante:
                if (zona[3] > cantidadIndividuos):
                    cooperar = reputacionNegativa
                else:
                    cooperar = reputacionPositiva
        if(emocion == 3):  # Al individuo lo domina la emocion de Esperanza: Busca ubicaciones con mayor cantidad de recursos
            for zona in zonasAlimentacionNegociante:
                if (zona[2] > capacidadZona):
                    cooperar = reputacionNegativa
                else:
                    cooperar = reputacionPositiva
        if(emocion == 4):  # Al individuo lo domina la emocion  de Miedo: Busca una ubicacion que contenga mayor capacidad y
                        # mayor cantidad individuos (Basicamente está desesperado por salir de la ubicacion actual)
            for zona in zonasAlimentacionNegociante:
                if (zona[2] > capacidadZona or zona[3] > cantidadIndividuos):
                    cooperar = reputacionNegativa
                else:
                    cooperar = reputacionPositiva
        
        if(cooperar):
            self.actualizarDataSet(1,personalidad[2],True,emocion)
        else:
            self.actualizarDataSet(1,personalidad[2],False,emocion)

        return cooperar

    """
        funcion que unifica la informacion del explorador con el negociador, si alguno de los dos tomó la desicion de traicionar
            la ubicacion de estos sitios  tomaran valores aleatorios
        los parametros de entrada serán un arreglo de dos posiciones... [indice del individuo, desicion(Cooperar Traicionar)]
    """

    def compartirInformacion(self, informacionExplorador, informacionNegociador):
        """ Se hace una copia de las zonas de alimentacion de cada individuo """
        zonaAlimentacionExploradorActual = []
        for zE in self.individuos_array[informacionExplorador[0]]['zonas_alimentacion']:
            zonaAlimentacionExploradorActual.append(
                [zE[0], zE[1], zE[2], zE[3], zE[4], zE[5], zE[6]])

        zonaAlimentacionNegociadorActual = []
        for zN in self.individuos_array[informacionNegociador[0]]['zonas_alimentacion']:
            zonaAlimentacionNegociadorActual.append(
                [zN[0], zN[1], zN[2], zN[3], zN[4], zN[5], zN[6]])

        """ Variables contienen la desicion tomada por los individuos de cooperar o no (True o False) """
        decisionExplorador = informacionExplorador[1]
        decisionNegociador = informacionNegociador[1]

        """ 
            Cuando el explorador decide cooperar, se actualiza el id de las zonas que 
            el conoce (para hacer seguimiento de quien dio la informacion)
            Se actualiza el valor de la posicion 5 (indica en que zona está situado el individuo)
        """
        if(decisionExplorador):
            for l in zonaAlimentacionExploradorActual:
                idExplorador = self.individuos_array[informacionExplorador[0]]['id']
                l[4] = idExplorador
                l[5] = False
                if(not self.existenciaConocimiento(l,self.individuos_array[informacionNegociador[0]]['zonas_alimentacion'])):
                    self.individuos_array[informacionNegociador[0]]['zonas_alimentacion'].append(l)

        """ 
            Cuando el explorador decide traicionar, las coordenas (x, y) de las zonas de alimentacion se generan aleatoriamente
        """
        if(not decisionExplorador):
            for o in zonaAlimentacionExploradorActual:
                idExplorador = self.individuos_array[informacionExplorador[0]]['id']
                o[0] = random.randint(0, len(self.mapa)-1)
                o[1] = random.randint(0, len(self.mapa)-1)
                o[4] = idExplorador
                o[5] = False
                self.individuos_array[informacionNegociador[0]]['zonas_alimentacion'].append(o)

        if(decisionNegociador):
            for j in zonaAlimentacionNegociadorActual:
                idNegociador = self.individuos_array[informacionNegociador[0]]['id']
                j[4] = idNegociador
                j[5] = False
                if(not self.existenciaConocimiento(j,self.individuos_array[informacionExplorador[0]]['zonas_alimentacion'])):
                    self.individuos_array[informacionExplorador[0]]['zonas_alimentacion'].append(j)

        if(not decisionNegociador):
            for t in zonaAlimentacionNegociadorActual:
                idNegociador = self.individuos_array[informacionNegociador[0]]['id']
                t[0] = random.randint(0, len(self.mapa)-1)
                t[1] = random.randint(0, len(self.mapa)-1)
                t[4] = idNegociador
                t[5] = False
                self.individuos_array[informacionExplorador[0]]['zonas_alimentacion'].append(t)
        else:
            pass
    
    """ Se verifica si la zona ya se encuentra registrada """
    def existenciaConocimiento(self, zona, zonasIndividuo):
        existe = False
        for x in zonasIndividuo:
            if(x[6]==zona[6]):
                existe = True
                return existe
        return existe

    # """ Se verifica si la ubicacion del depredador ya se encuentra registrada """
    # def existenciaConocimientoDepredador(self, depredador, depredadoresIndividuo):
    #     existe = False
    #     for x in depredadoresIndividuo:
    #         if(x[0]==depredador[0] and x[1]==depredador[1]):
    #             existe = True
    #             return existe
    #     return existe
    '''
       funcion que asigna la emoción a los individuos pobladores
    '''

    def difundirInformacion(self, individuo):
        for poblador in self.individuos_array:
            if(poblador['tribu'] == individuo['tribu']):
                if(poblador['id'] == individuo['id']):
                    pass
                else:
                    self.añadirZonas(individuo['zonas_alimentacion'], poblador['zonas_alimentacion'])

    '''
        Se encarga de asignar la emoción del individuo de acuerdo
        al modelo emocional planteado
    '''
    def asignarEmocion(self):

        for poblador in self.individuos_array:

            # para las siguientes dos var se van a manejar 0: negativo 1:positivo
            dominancia = 0
            placer = 0

            positivo_placer = 0
            negativo_placer = 0
            positivo_dominancia = 0
            negativo_dominancia = 0
            
            if(poblador['vida'] > 0):
                # calculando el placer
                parejas = self.parejasIndividuo(poblador)
                if(parejas):
                    positivo_placer = (len(parejas) - 1) * poblador['personalidad'][0]
                    negativo_placer = ((len(poblador['zonas_depredadores'])-1)* 2) * poblador['personalidad'][1]
                else:
                    positivo_placer = 0
                    negativo_placer = ((len(poblador['zonas_depredadores'])-1)* 2) * poblador['personalidad'][1]


           
                if(positivo_placer >= negativo_placer):
                    placer = 1
                else:
                    placer = 0

                
                visitadas = 0
                no_visitadas = 0
                for zona in poblador['zonas_alimentacion']:
                    if(zona):  # si la lista contiene algo da true sino false
                        if(zona[4] == -1):
                            visitadas += 1
                        else:
                            no_visitadas += 1

                positivo_dominancia = visitadas * poblador['personalidad'][0]
                negativo_dominancia = no_visitadas * poblador['personalidad'][1]

                

                if(positivo_dominancia >= negativo_dominancia):
                    dominancia = 1
                else:
                    dominancia = 0
                
                # calculando la emoción a partir del la domnancia y el placer

                if((dominancia == 1) and (placer == 1)):
                    poblador['emocion'] = 1
                elif((dominancia == 1) and (placer == 0)):
                    poblador['emocion'] = 2
                elif((dominancia == 0) and (placer == 1)):
                    poblador['emocion'] = 3
                else:
                    poblador['emocion'] = 4
                    

    def take_second(self, elem):
        return elem[2]        
    """
        La decision de desplazarse o no es de acuerdo a los miedos que tiene el individuo
        Una vez se decide que miedo va acondicionar la decision, la funcion retorna un arreglo de 4 posiciones(response)
        en el cual: 1. la posicion 1 es un boleano indicando si el individuo va a cambiar de destino(True) o False si decide mantener el destino actual
                    2 y 3. Pos x y y del nuevo destino del individuo
                    4. String con la infomarcion de cual miedo influyó la decision del individuo
    """
    def nuevoDestino(self, individuo):
        #remuevo del conocimiento del individuo las zonas que sean 0 en su capacidad
        for zona in individuo['zonas_alimentacion']:
            if(zona[2] <= 0):
                individuo['zonas_alimentacion'].remove(zona)
        

        geneticaIndividuo = individuo['genetica']
        informacionZonasAlimentacion = individuo['zonas_alimentacion']
        informacionDepredadores = individuo['zonas_depredadores']
        '''
        activacionE = round((self.varibales_control['prob_explorador']/100), 2)
        explorador = random.random()
        nacer_explorador = True if explorador <= activacionE else False
        '''
        categoria = {'p': round(geneticaIndividuo['panico']/100,2), 
                    'd': round(geneticaIndividuo['desplazamiento']/100,2), 
                    'a': round(geneticaIndividuo['agrupamiento']/100,2)
        }
        #print(geneticaIndividuo)
        aleatorio = random.random()
        aleatorio_sub = random.random()
        #print('aleatorio --> ', aleatorio)
        # ordeno las categorias de menor a mayor  sale en forma de lista [(clave,value),...]
        categoria_ordenada = sorted(categoria.items(), key=operator.itemgetter(1))
        if(aleatorio <= categoria_ordenada[0][1]):
            if(categoria_ordenada[0][0] == 'p'):
                subPd = round(geneticaIndividuo['panico_depredador']/100,2)
                if(aleatorio_sub>=subPd):
                    subCategoriaSeleccionada = 'Panico_Comida'
                else:
                    subCategoriaSeleccionada = 'Panico_Depredador'
            elif(categoria_ordenada[0][0] == 'd'):
                subT1 = round(geneticaIndividuo['tipo_1']/100,2)
                if(aleatorio_sub>=subT1):
                    subCategoriaSeleccionada = 'Tipo_2'
                else:
                        subCategoriaSeleccionada = 'Tipo_1'
            else:
                subAg = round(geneticaIndividuo['agruparse']/100,2)
                if(aleatorio_sub>=subAg):
                    subCategoriaSeleccionada = 'Aislarse'
                else:
                    subCategoriaSeleccionada = 'Agruparse'
            
        elif( (aleatorio > categoria_ordenada[0][1]) and (aleatorio <= categoria_ordenada[1][1])):
            if(categoria_ordenada[0][0] == 'p'):
                subPd = round(geneticaIndividuo['panico_depredador']/100,2)
                if(aleatorio_sub>=subPd):
                    subCategoriaSeleccionada = 'Panico_Comida'
                else:
                    subCategoriaSeleccionada = 'Panico_Depredador'
            elif(categoria_ordenada[0][0] == 'd'):
                subT1 = round(geneticaIndividuo['tipo_1']/100,2)
                if(aleatorio_sub>=subT1):
                    subCategoriaSeleccionada = 'Tipo_2'
                else:
                        subCategoriaSeleccionada = 'Tipo_1'
            else:
                subAg = round(geneticaIndividuo['agruparse']/100,2)
                if(aleatorio_sub>=subAg):
                    subCategoriaSeleccionada = 'Aislarse'
                else:
                    subCategoriaSeleccionada = 'Agruparse'
            
        else:
            if(categoria_ordenada[0][0] == 'p'):
                subPd = round(geneticaIndividuo['panico_depredador']/100,2)
                if(aleatorio_sub>=subPd):
                    subCategoriaSeleccionada = 'Panico_Comida'
                else:
                    subCategoriaSeleccionada = 'Panico_Depredador'
            elif(categoria_ordenada[0][0] == 'd'):
                subT1 = round(geneticaIndividuo['tipo_1']/100,2)
                if(aleatorio_sub>=subT1):
                    subCategoriaSeleccionada = 'Tipo_2'
                else:
                        subCategoriaSeleccionada = 'Tipo_1'
            else:
                subAg = round(geneticaIndividuo['agruparse']/100,2)
                if(aleatorio_sub>=subAg):
                    subCategoriaSeleccionada = 'Aislarse'
                else:
                    subCategoriaSeleccionada = 'Agruparse'
        if(subCategoriaSeleccionada == 'Panico_Depredador'):
            auxDistancia = 0
            mejorZona = []
            # ORDENA LAS ZONAS POR ZONA[2] CREO QUE ES LA CAPACIDAD DE MAYOR A MENOR
            sorted_zonas = sorted(informacionZonasAlimentacion, key=lambda zona: zona[2], reverse=True)

            #? De las primeras 5 zonas de alimentación se observa cual está más alejada de los depredadores y 
            #? ese será el nuevo destino del individuo
            for i, zona in enumerate(sorted_zonas):
                #print(zona)
                distancia = 0
                for depredador in informacionDepredadores:
                    distancia += self.distance([zona[0], zona[1]],
                                               [depredador[0], depredador[1]])
                if(distancia > auxDistancia):
                    mejorZona = zona
                    auxDistancia = distancia
                if(i==5):
                    break

            if(mejorZona):
                if(mejorZona[5]):
                    return [False, -1, -1, "Panico Depredador"]
                else:
                    self.variables_test['Panico_Depredador'] += 1
                    return [True,
                            mejorZona[0],
                            mejorZona[1],
                            "Panico Depredador"]
        if(subCategoriaSeleccionada == 'Panico_Comida'):
            # Se ordenan las zonas (de acuerdo a su capacidad)de mayor a menor y se elige la que esté ubicada
            # en la posicion 0 
            sorted_zonas = sorted(informacionZonasAlimentacion, key=lambda zona: zona[2], reverse=True)
            if(len(sorted_zonas)>0):
                mejorZona = sorted_zonas[0]
                if(mejorZona[5]):
                    return [False, -1, -1, "Panico Comida"]
                else:
                    self.variables_test['Panico_Comida'] += 1
                    return [True, mejorZona[0], mejorZona[1],"Panico Comida"]
        if(subCategoriaSeleccionada == 'Tipo_1'):
            mejorZona = -1
            menorDistancia = 1000
            for i, zona in enumerate(informacionZonasAlimentacion):
                if(zona[5]):
                    pass
                else:
                    distanciaZona = self.distance(
                        individuo['pos_actual'], [zona[0], zona[1]])
                    if(distanciaZona < individuo['vida']):
                        if(distanciaZona < menorDistancia):
                            menorDistancia = distanciaZona
                            mejorZona = i

            if(mejorZona == -1):
                return [False, -1, -1, "Tipo 1"]
            else:
                self.variables_test['Tipo_1'] += 1
                return [True,
                        informacionZonasAlimentacion[mejorZona][0],
                        informacionZonasAlimentacion[mejorZona][1],
                        "Tipo 1"]
        if(subCategoriaSeleccionada == 'Tipo_2'):
            mejorZona = -1
            menorDistancia = 1000
            for i, zona in enumerate(informacionZonasAlimentacion):
                if(zona[5]):
                    pass
                else:
                    distanciaZona = self.distance(
                        individuo['pos_actual'], [zona[0], zona[1]])
                    if(distanciaZona < individuo['vida']):
                        if(distanciaZona < menorDistancia):
                            menorDistancia = distanciaZona
                            mejorZona = i

            if(mejorZona == -1):
                self.variables_test['Tipo_2'] += 1
                return [True,
                        random.randint(0, len(self.mapa)-1),
                        random.randint(0, len(self.mapa)-1),
                        "Tipo 2"]
            else:
                self.variables_test['Tipo_2'] += 1
                return [True,
                        informacionZonasAlimentacion[mejorZona][0],
                        informacionZonasAlimentacion[mejorZona][1],
                        "Tipo 2"]
        if(subCategoriaSeleccionada == 'Agruparse'):
            mejorZona = 0
            mayorCantidadIndividuos = 0
            for i, zona in enumerate(informacionZonasAlimentacion):
                if(zona[3] > mayorCantidadIndividuos):
                    mayorCantidadIndividuos = zona[3]
                    mejorZona = i
            if(informacionZonasAlimentacion):
                if(informacionZonasAlimentacion[mejorZona][5]):
                    return [False, -1, -1, "Agruparse"]
                else:
                    self.variables_test['Agruparse'] += 1
                    return [True,
                            informacionZonasAlimentacion[mejorZona][0],
                            informacionZonasAlimentacion[mejorZona][1],
                            "Agruparse"]
        if(subCategoriaSeleccionada == 'Aislarse'):
            mejorZona = 0
            mayorCantidadIndividuos = 1000
            for i, zona in enumerate(informacionZonasAlimentacion):
                if(zona[3] < mayorCantidadIndividuos):
                    mayorCantidadIndividuos = zona[3]
                    mejorZona = i
            if(informacionZonasAlimentacion):
                if(informacionZonasAlimentacion[mejorZona][5]):
                    return [False, -1, -1, "Aislarse"]
                else:
                    self.variables_test['Aislarse'] += 1
                    return [True,
                            informacionZonasAlimentacion[mejorZona][0],
                            informacionZonasAlimentacion[mejorZona][1],
                            "Aislarse"]
        return [False, 0, 0, "Ninguna Eleccion"]

    """
        Funcion que  actualiza todas las variables booleanas de las zonas propias en false
        indicando que ya pertenece a ninguna zona
    """
    def auxSinZonaAlimentacion(self, individuo):
        for zona in individuo['zonas_alimentacion']:
            zona[5] = False

    """
        Retorna la pos de la zona de alimentacion actual (Su casa)
    """
    def posMiZonaAlimentacion(self, individuo):
        response = [-1, -1]
        for zona in individuo['zonas_alimentacion']:
            if(zona[5]):
                response = [zona[0], zona[1]]
        return response

    """
        Actualiza determinada zona de alimentacion para indicar que es su nueva casa
    """
    def actualizarMiZonaAlimentacion(self, individuo, posX, posY):
        for zona in individuo['zonas_alimentacion']:
            if(zona[0] == posX and zona[1] == posY):
                zona[5] = True
                break
    '''
        Identifica si se ha dado click a un individuo durante la simulación
        y de ocurrir muestra la información de un individuo en una ventana emergente.

        pixlX, pixlY : posición donde el mouse hizo click
    '''
    def mostrarInformacion(self, pixlX, pixlY):
        posX = math.floor((20*pixlX)/620)
        posY = math.floor((20*pixlY)/620)

        if(pixlX > 650 or pixlY > 650):
            pass
        else:
            posArray = self.mapa[posY][posX][1]
            tipo = self.mapa[posY][posX][0]
            #print(posArray, " Tipo ", tipo)
            if(tipo == 3):
                #print("X: ", posY, " Y: ", posX)
                individuo = self.individuos_array[posArray]
                emocion = individuo['emocion']
                personalidad = individuo['personalidad']
                if(emocion == 1):
                    nombreEmocion = "Felicidad"
                if(emocion == 2):
                    nombreEmocion = "Ira"
                if(emocion == 3):
                    nombreEmocion = "Esperanza"
                if(emocion == 4):
                    nombreEmocion = "Miedo"
               
                informacion = (" Id: " + str(posArray) + "\n" + " Emotion: " + nombreEmocion + "\n" + " Pos Currently: [" + str(individuo['pos_actual'][1]) + "] [" + str(
                    individuo['pos_actual'][0]) + "] \n" + " Destination: [" + str(individuo['destino'][1]) + "] [" + str(
                    individuo['destino'][0]) + "]\n"  + " Reproductions: " + str(individuo['reproducciones'])+"\n"+" Life: " + str(individuo['vida']) + "\n" + " Tribe: " + str(individuo['tribu']) + "\n" +" GoHome: " + str(
                    individuo['goHome']) + "\n" + " Reputation: " + str(individuo['reputacion']) + "\n" + " Negative Reputation: " + str(
                    individuo['reputacionNegativa']) + "\n" + " Personality: " + personalidad[2] + "\n\n"+ "________ Genetics ________  " + "\n\n"+
                    'Panic: '+str(individuo['genetica']['panico']) + "% (Predator panic: "+str(individuo['genetica']['panico_depredador']) +"% - feeding panic: "+str(individuo['genetica']['panico_comida'])+ "%)\n" +
                    "Grouping: "+str(individuo['genetica']['agrupamiento'])+"% (group: "+ str(individuo['genetica']['agruparse']) + "% - Isolate: "+ str(individuo['genetica']['aislarse'])+"%)\n" +
                    "displacement: "+ str(individuo['genetica']['desplazamiento']) + "% (Tiype_1: " +str(individuo['genetica']['tipo_1']) + "% - Type_2: "+str(individuo['genetica']['tipo_2']) + "%)\n\n"+ "________ knowledge ________  " +"\n\n"+ "Feeding: "+str(
                    len(individuo['zonas_alimentacion'])) +"\n"+ "Predators: "+str(len(individuo['zonas_depredadores'])))
                       
                eg.msgbox(
                    msg=informacion, title='Check: Inhabitant data', ok_button='Close')

            else:
                pass

                
    '''
        Se encarga de disminuir la vida de los individuos o depredadores
    '''
    def disminuirVida(self):
        for depredador in self.depredadores_array:
            if(depredador['vida'] > 0):
                depredador['vida'] = depredador['vida'] - 1
                if(depredador['vida'] < 1):
                    posX = depredador['pos'][0]
                    posY = depredador['pos'][1]
                    self.mapa[posX][posY] = [1,-1]
                    self.variables_test['depredadores'] -= 1
                else:
                    pass
        for individuo in self.individuos_array:
            if(individuo['vida'] > 0):
                if(individuo['talimentacion'] > 0):
                    individuo['talimentacion'] -= 1
                else:
                    individuo['vida'] = individuo['vida'] - 1
                    if(individuo['vida'] <= 0):
                        posX = individuo['pos_actual'][0]
                        posY = individuo['pos_actual'][1]
                        self.mapa[posX][posY] = [1,-1]
                        individuo['kmeans'] = False
                        individuo['tribu'] = 0
                        self.individuosMuertos += 1
                        self.variables_test['individuosMuertos'] += 1
                        self.individuosVivos -= 1
                        self.variables_test['individuosVivos'] -= 1
                        self.actualizarDataSet(0, individuo['personalidad'][2], False, individuo['emocion'])
                    else:
                        pass
    
    """
        Funcion que toma cada depredador ubicado en el mapa y revisa si se encuentra en reposo o no.
        En caso de no estar en reposo examina si hay algun individuo cerca y procede a atacarlo
        La potencia de la vida es proporcional a la cantidad de vida que le falte, una vez realizado el ataque entra en modo de reposo
        si el individuo muere, es eliminado del mapa
    """
    def alimentarDepredador(self):
        for depredador in self.depredadores_array:
            if(depredador['vida'] > 0):
                if(depredador['tiempo_reposo'] <= 0):
                    posiblesPresas = self.individuosCercanos(depredador['pos'])
                    if(posiblesPresas):
                        objetivo = random.choice(posiblesPresas)
                        
                        individuo = self.individuos_array[objetivo]
                        potenciaAtaque = (self.varibales_control['time_life']//4) - depredador['vida']
                        depredador['vida'] += potenciaAtaque
                        individuo['vida'] = individuo['vida'] - potenciaAtaque
                        depredador['tiempo_reposo'] = 3
                        if(individuo['vida'] < 1):
                            posX = individuo['pos_actual'][0]
                            posY = individuo['pos_actual'][1]
                            self.mapa[posX][posY] = [1,-1]
                            individuo['kmeans'] = False
                            individuo['tribu'] = 0
                            self.individuosMuertos += 1
                            self.variables_test['individuosMuertos'] += 1
                            self.individuosVivos -= 1
                            self.variables_test['individuosVivos'] -= 1
                            self.actualizarDataSet(0, individuo['personalidad'][2], False, individuo['emocion'])

                else:
                    depredador['tiempo_reposo'] = depredador['tiempo_reposo']-1
    
   
    '''
        DESCRIBIR FUNCIÓN
    '''
    def reputacionNegativa(self, posDestino, individuo):  
        for zona in individuo['zonas_alimentacion']:
            if(zona[0] == posDestino[0] and zona[1] == posDestino[1]):
                if(zona[4] != -1):
                    if(self.mapa[posDestino[0]][posDestino[1]][0] != 2):
                        informante = self.individuos_array[zona[4]]
                        if(informante['vida']>0):
                            informante['reputacionNegativa'] += 1
                            zona[4] = -1

    
    '''
        DESCRIBIR FUNCIÓN
    '''
    def almacenarDatos(self):
        x = datetime.datetime.now()
        nameFile =  "data-" + "%s%s%s" % (x.hour, x.minute,x.second) + ".txt"
        file_path = eg.filesavebox(
                                title="Save File",
                                default=nameFile,
                                filetypes=["*.*"])                                    
        if(file_path is None):
            return False
        else:
            archivo = open(file_path, "a")
            cabezera = ("IndividuosVivos" + "\t" + "IndividuosMuertos"  + "\t" + 'depredadores' + "\t" + 'zonasAlimentacion' + "\t" + 
                'neutralMuertos' + "\t" + 'neutralVivos' + "\t"  + 'neutralConocimientoZonas' + "\t" + 'neutralConocimientoDepredadores' + "\t"+'neutralFelicidadH'+ "\t"+'neutralFelicidadM'+ "\t"+ 'neutralIraH'+ "\t"+ 'neutralIraM'+ "\t"+'neutralMiedoH'+ "\t"+'neutralMiedoM'+ "\t"+'neutralEsperanzaH'+ "\t"+'neutralEsperanzaM'+ "\t" + 
                'optimistasMuertos'+ "\t"+'optimistasVivos'+ "\t"+ 'optimistasConocimientoZonas' + "\t" + 'optimistasConocimientoDepredadores' + "\t"+ 'optimistasFelicidadH'+ "\t"+'optimistasFelicidadM'+ "\t"+'optimistasIraH'+ "\t"+'optimistasIraM'+ "\t"+'optimistasMiedoH'+ "\t"+'optimistasMiedoM'+ "\t"+'optimistasEsperanzaH'+ "\t"+'optimistasEsperanzaM'+ "\t"+
                'muyOptimistasMuertos'+ "\t"+'muyOptimistasVivos'+ "\t" + 'muyOptimistasConocimientoZonas' + "\t" + 'muyOptimistasConocimientoDepredadores' + "\t"+'muyOptimistasFelicidadH'+ "\t"+'muyOptimistasFelicidadM'+ "\t"+'muyOptimistasIraH'+ "\t"+'muyOptimistasIraM'+ "\t"+'muyOptimistasMiedoH'+ "\t"+'muyOptimistasMiedoM'+ "\t"+'muyOptimistasEsperanzaH'+ "\t"+'muyOptimistasEsperanzaM'+ "\t"+
                'pesimistasMuertos'+ "\t"+'pesimistasVivos'+ "\t"+ 'pesimistasConocimientoZonas' + "\t" + 'pesimistasConocimientoDepredadores' + "\t"+'pesimistasFelicidadH'+ "\t"+'pesimistasFelicidadM'+ "\t"+'pesimistasIraH'+ "\t"+'pesimistasIraM'+ "\t"+'pesimistasMiedoH'+ "\t"+'pesimistasMiedoM'+ "\t"+'pesimistasEsperanzaH'+ "\t"+'pesimistasEsperanzaM'+ "\t"+
                'muyPesimistasMuertos'+ "\t"+'muyPesimistasVivos'+ "\t" + 'muyPesimistasConocimientoZonas' + "\t" + 'muyPesimistasConocimientoDepredadores' +"\t"+'muyPesimistasFelicidadH'+ "\t"+'muyPesimistasFelicidadM'+ "\t"+'muyPesimistasIraH'+ "\t"+'muyPesimistasIraM'+ "\t"+'muyPesimistasMiedoH'+ "\t"+'muyPesimistasMiedoM'+ "\t"+'muyPesimistasEsperanzaH'+ "\t"+'muyPesimistasEsperanzaM' + "\t" + 
                'Panico_Depredador'+ "\t"+ 'Panico_Comida'+ "\t"+ 'Tipo_1'+ "\t"+ 'Tipo_2'+ "\t"+ 'Agruparse'+ "\t"+ 'Aislarse'+"\n")
            archivo.write(cabezera) 
            generaciones = self.data_set
            for generacion in generaciones:
                listaValores = list(generacion)
                for element in listaValores:
                    archivo.write(str(element) + "\t")
                archivo.write("\n")
            archivo.close()

            eg.msgbox(msg="The data is saved", title='Control: DATATEST', ok_button='Close')
    
    
    '''
        DESCRIBIR FUNCIÓN
    '''
    def actualizarDataSet(self, tipo, personalidad, indicador, emocion):
        """ Si el parametro tipo es 0: se actualizaran las variables de vida
                        tipo es 1: se actualizaran las variables de honestidad
        indicador será un boleano que determinar si el individuo (nacio o murió) - (fue honesto o mintió)"""
        if(tipo==0):
            if(personalidad == "Neutral"):
                if(indicador):
                    self.variables_test['neutralVivos'] += 1
                else:
                    self.variables_test['neutralMuertos'] += 1
            if(personalidad == "Optimista"):
                if(indicador):
                    self.variables_test['optimistasVivos'] += 1
                else:
                    self.variables_test['optimistasMuertos'] +=1
            if(personalidad == "Muy Optimista"):
                if(indicador):
                    self.variables_test['muyOptimistasVivos'] += 1
                else:
                    self.variables_test['muyOptimistasMuertos'] +=1
            if(personalidad == "Pesimista"):
                if(indicador):
                    self.variables_test['pesimistasVivos'] += 1
                else:
                    self.variables_test['pesimistasMuertos'] +=1
            if(personalidad == "Muy Pesimista"):
                if(indicador):
                    self.variables_test['muyPesimistasVivos'] += 1
                else:
                    self.variables_test['muyPesimistasMuertos'] +=1
        if(tipo==1):
            if(personalidad == "Neutral"):
                if(indicador):#Si el indicador es true se actualizan las variables de honestidad
                    if(emocion==1):
                        self.variables_test['neutralFelicidadH'] += 1
                    elif(emocion==2):
                        self.variables_test['neutralIraH'] += 1
                    elif(emocion==4):
                        self.variables_test['neutralMiedoH'] += 1
                    else:#Esperanza
                        self.variables_test['neutralEsperanzaH'] += 1
                else:
                    if(emocion==1):
                        self.variables_test['neutralFelicidadM'] += 1
                    elif(emocion==2):
                        self.variables_test['neutralIraM'] += 1
                    elif(emocion==4):
                        self.variables_test['neutralMiedoM'] += 1
                    else:#Esperanza
                        self.variables_test['neutralEsperanzaM'] += 1
            if(personalidad == "Optimista"):
                if(indicador):
                    if(emocion==1):
                        self.variables_test['optimistasFelicidadH'] += 1
                    elif(emocion==2):
                        self.variables_test['optimistasIraH'] += 1
                    elif(emocion==4):
                        self.variables_test['optimistasMiedoH'] += 1
                    else:#Esperanza
                        self.variables_test['optimistasEsperanzaH'] += 1
                else:
                    if(emocion==1):
                        self.variables_test['optimistasFelicidadM'] += 1
                    elif(emocion==2):
                        self.variables_test['optimistasIraM'] += 1
                    elif(emocion==4):
                        self.variables_test['optimistasMiedoM'] += 1
                    else:#Esperanza
                        self.variables_test['optimistasEsperanzaM'] += 1
            if(personalidad == "Muy Optimista"):
                if(indicador):
                    if(emocion==1):
                        self.variables_test['muyOptimistasFelicidadH'] += 1
                    elif(emocion==2):
                        self.variables_test['muyOptimistasIraH'] += 1
                    elif(emocion==4):
                        self.variables_test['muyOptimistasMiedoH'] += 1
                    else:#Esperanza
                        self.variables_test['muyOptimistasEsperanzaH'] += 1
                else:
                    if(emocion==1):
                        self.variables_test['muyOptimistasFelicidadM'] += 1
                    elif(emocion==2):
                        self.variables_test['muyOptimistasIraM'] += 1
                    elif(emocion==4):
                        self.variables_test['muyOptimistasMiedoM'] += 1
                    else:#Esperanza
                        self.variables_test['muyOptimistasEsperanzaM'] += 1
            if(personalidad == "Pesimista"):
                if(indicador):
                    if(emocion==1):
                        self.variables_test['pesimistasFelicidadH'] += 1
                    elif(emocion==2):
                        self.variables_test['pesimistasIraH'] += 1
                    elif(emocion==4):
                        self.variables_test['pesimistasMiedoH'] += 1
                    else:#Esperanza
                        self.variables_test['pesimistasEsperanzaH'] += 1
                else:
                    if(emocion==1):
                        self.variables_test['pesimistasFelicidadM'] += 1
                    elif(emocion==2):
                        self.variables_test['pesimistasIraM'] += 1
                    elif(emocion==4):
                        self.variables_test['pesimistasMiedoM'] += 1
                    else:#Esperanza
                        self.variables_test['pesimistasEsperanzaM'] += 1
            if(personalidad == "Muy Pesimista"):
                if(indicador):
                    if(emocion==1):
                        self.variables_test['muyPesimistasFelicidadH'] += 1
                    elif(emocion==2):
                        self.variables_test['muyPesimistasIraH'] += 1
                    elif(emocion==4):
                        self.variables_test['muyPesimistasMiedoH'] += 1
                    else:#Esperanza
                        self.variables_test['muyPesimistasEsperanzaH'] += 1
                else:
                    if(emocion==1):
                        self.variables_test['muyPesimistasFelicidadM'] += 1
                    elif(emocion==2):
                        self.variables_test['muyPesimistasIraM'] += 1
                    elif(emocion==4):
                        self.variables_test['muyPesimistasMiedoM'] += 1
                    else:#Esperanza
                        self.variables_test['muyPesimistasEsperanzaM'] += 1

    '''
        DESCRIBIR FUNCIÓN
    '''
    def resetDataSet(self):
        self.variables_test = {
            'individuosVivos': self.variables_test['individuosVivos'], 'individuosMuertos': self.variables_test['individuosMuertos'],
            'depredadores': self.variables_test['depredadores'],'zonasAlimentacion': self.variables_test['zonasAlimentacion'],
            'neutralMuertos': 0,'neutralVivos': 0, 'neutralConocimientoZonas': 0, 'neutralConocimientoDepredadores': 0, 'neutralFelicidadH': 0,'neutralFelicidadM': 0, 'neutralIraH': 0, 'neutralIraM': 0,'neutralMiedoH': 0,'neutralMiedoM': 0,'neutralEsperanzaH': 0,'neutralEsperanzaM': 0,
            'optimistasMuertos': 0,'optimistasVivos': 0, 'optimistaConocimientoZonas': 0, 'optimistaConocimientoDepredadores': 0,'optimistasFelicidadH': 0,'optimistasFelicidadM': 0,'optimistasIraH': 0,'optimistasIraM': 0,'optimistasMiedoH': 0,'optimistasMiedoM': 0,'optimistasEsperanzaH': 0,'optimistasEsperanzaM': 0,
            'muyOptimistasMuertos': 0,'muyOptimistasVivos': 0, 'muyOptimistaConocimientoZonas': 0, 'muyOptimistaConocimientoDepredadores': 0, 'muyOptimistasFelicidadH': 0,'muyOptimistasFelicidadM': 0,'muyOptimistasIraH': 0,'muyOptimistasIraM': 0,'muyOptimistasMiedoH': 0,'muyOptimistasMiedoM': 0,'muyOptimistasEsperanzaH': 0,'muyOptimistasEsperanzaM': 0,                
            'pesimistasMuertos': 0,'pesimistasVivos': 0, 'pesimistaConocimientoZonas': 0, 'pesimistaConocimientoDepredadores': 0,'pesimistasFelicidadH': 0,'pesimistasFelicidadM': 0,'pesimistasIraH': 0,'pesimistasIraM': 0,'pesimistasMiedoH': 0,'pesimistasMiedoM': 0,'pesimistasEsperanzaH': 0,'pesimistasEsperanzaM': 0,
            'muyPesimistasMuertos': 0,'muyPesimistasVivos': 0, 'MuyPesimistaConocimientoZonas': 0, 'MuyPesimistaConocimientoDepredadores': 0, 'muyPesimistasFelicidadH': 0,'muyPesimistasFelicidadM': 0,'muyPesimistasIraH': 0,'muyPesimistasIraM': 0,'muyPesimistasMiedoH': 0,'muyPesimistasMiedoM': 0,'muyPesimistasEsperanzaH': 0,'muyPesimistasEsperanzaM': 0,
            'Panico_Depredador': 0, 'Panico_Comida': 0, 'Tipo_1': 0, 'Tipo_2': 0, 'Agruparse': 0, 'Aislarse': 0
    }

    
    '''
        Carga y lee el archivo de los valores de configuración 
    '''
    def archivoCoreccto(self):
 

        file_path = eg.fileopenbox(
                                title="Choose File",
                                default=os.getcwd()+'/*.txt',
                                filetypes=["*.txt"])
        if(file_path is None):
            return False
        else:
            file = open(file_path,"r")
            contenido = file.read()
            file.close()
            lines = contenido.split(sep='\n')

            #por si tiene enter de más los elimino
            while('' in lines):
                lines.remove('')
                print(len(lines))
            
            if(len(lines)==9):
                todos_numeros = True
                for l in lines:
                    if(l.isdigit() and len(l)<=2):
                        #pasa al siguiente elemento de iteración
                        continue

                    else:
                        eg.msgbox ( msg = "The file must have numbers (Max 2 digit)." ,  title = "Error" ,  ok_button = "Ok" )
                        todos_numeros = False
                        return False
                
                if(todos_numeros):
                    # debe ponerse esto antes de cargar las variables
                    self.varibales_control['time_life'] = int(lines[0])
                    self.varibales_control['reproducciones'] = int(lines[1])
                    self.varibales_control['tespera'] = int(lines[2])
                    self.varibales_control['talimentacion'] = int(lines[3])
                    self.varibales_control['numero_aparicion'] = int(lines[4])
                    self.varibales_control['tasas_aparicion'] = int(lines[5])
                    self.varibales_control['cantidad_recursos'] = int(lines[6])
                    self.varibales_control['tamano_poblacion'] = int(lines[7])
                    self.varibales_control['prob_explorador'] = int(lines[8])
                    
                    #cargar variables inciales 
                    self.cargarValoresIniciales()
                    return True

            else:
                #MOstrar una ventana indicando el error 
                eg.msgbox ( msg = "The file must have: \n-Numbers\n-9 number typed in column" ,  title = "Error" ,  ok_button = "Ok" )
                return False


    '''
        DESCRIPCIÓN FUNCIÓN
    '''  
    def actualizarPosicionHijos(self):
        for hijo in self.hijos_array:
            self.mapa[hijo['pos_actual'][0]][hijo['pos_actual'][1]][0] = 3

    '''
        DESCRIPCIÓN FUNCIÓN
    '''  
    def mostrarIndividuos(self, indiviuo):
        print("Id: ", indiviuo['id'], "pos_actual: ", indiviuo['pos_actual'], "pos_destino: ", indiviuo['destino'],
                  " vida: ", indiviuo['vida'], "Zonas: ", indiviuo['zonas_alimentacion'])
    
    '''
        DESCRIPCIÓN FUNCIÓN
    '''                  
    def mostrarIndividuosAll(self):
        print("+*************Todos los indiviudos*********+")
        for indiviuo in self.individuos_array:
            print("Id: ", indiviuo['id'], "pos_actual: ", indiviuo['pos_actual'], "pos_destino: ", indiviuo['destino'],
                  " vida: ", indiviuo['vida'], "Zonas: ", indiviuo['zonas_alimentacion'], "Depredadores: ", indiviuo['zonas_depredadores'], "talimentacion:", 'talimentacion: ', indiviuo['talimentacion'])
        print("************************************")
            
    '''
        Se encarga de visualizar el panel que se indica como activo en la ventana.

        PARAMETROS
        - panel: es un parámetro de tipo string que representa el nombre del panel
    '''  
    def mostrarPanel(self, panel_activo):
        if(panel_activo == "simulacion"): 
            self.ventana.blit(self.imagen_panel_simulacion, (651, 0)) 
            self.dibujar_botones(self.lista_btn_simulacion)

        elif(panel_activo == "opciones" ):
            self.ventana.blit(self.imagen_panel_configuracion, (651, 0))   
            self.dibujar_botones(self.lista_btn_opciones) 
        
        elif(panel_activo == "inicio" ):
            self.ventana.blit(self.imagen_panel_inicio, (0, 0))   
            self.dibujar_botones(self.lista_btn_inicio) 

        elif(panel_activo == "entorno"):
            self.ventana.blit(self.imagen_panel_entorno, (651, 0)) 
            self.dibujar_botones(self.lista_btn_entorno)
            self.dibujar_input(self.input_entorno)
        
        elif(panel_activo == "individuo"):
            self.ventana.blit(self.imagen_panel_individuo, (651, 0)) 
            self.dibujar_botones(self.lista_btn_individuo)
            self.dibujar_input(self.input_individuos)
            
        
        elif(panel_activo == "alimentacion"):
            self.ventana.blit(self.imagen_panel_alimentacion, (651, 0)) 
            self.dibujar_botones(self.lista_btn_alimentacion)
            self.dibujar_input(self.input_alimentacion)
            

        elif(panel_activo == "depredador"):
            self.ventana.blit(self.imagen_panel_depredador, (651, 0)) 
            self.dibujar_botones(self.lista_btn_depredador)
            self.dibujar_input(self.input_depredadores)
        
        elif(panel_activo == "error-file-numerico"):
            self.ventana.blit(self.imagen_panel_depredador, (651, 0)) 
            self.dibujar_botones(self.lista_btn_depredador)
            self.dibujar_input(self.input_depredadores)

    '''
        Se encarga de verificar si los inputs de una lista contiene información,
        es decir, fueron digitados y es de una longitud de màximo dos dígitos

        RETURN
        .True: si encuentra un campo vacío
        -False: si todos los inputs estan digitados
    '''
    def inputsVacios(self, lista):
        for campo in lista:
            if (self.varibales_control[campo['name']] == 0):
                eg.msgbox ( msg = "All fields must have numbers" ,  
                                title = "Fields error" ,  ok_button = "Ok" )
                return True 
            if(len(str(self.varibales_control[campo['name']])) > 2):
                eg.msgbox ( msg = "It is only allowed to type numbers of maximum two digits",  
                                title = "Fields error" ,  ok_button = "Ok" )
                return True
        return False

    '''
        Se encarga de reestrablecer los valores iniciales de las variables globales
        que intervienen en la simulación con el objetivo de poder llevar a cabo
        las acciones de los botones detener y reiniciar. 
        PARAMETRO:
        -acción : indiciar la diferencia en las acciones de detener(false) y reiniciar (true)


    '''
    def reiniciarVariables(self, accion):
        self.pause_simulacion = True
        self.velocidad = 2000
        self.velocidad_pause = 0
        self.generacion = 0
        self.dimension = 0
        self.mapa = []
        self.recursosIniciales = True
        self.matrizdispersa = []
        self.individuosIniciales = True
        self.maximo_exploradores = 5
        self.individuosVivos = 0
        self.individuosMuertos = 0
        self.individuos_array = []
        self.hijos_array = []
        self.idIndividuos = 0
        self.zonasAlimentacion_array = []
        self.idZonaAlimentacion = 0
        self.depredadores_array = []
        self.idDepredadores = 0
        self.textoDigitado = ""   
        self.data_set = []
        self.tasaAparicionZonas = 0
        self.tasaAparicionDepredadores = 0
        #Variables del archivo de pruebas
        self.variables_test['individuosVivos'] = 0
        self.variables_test['individuosMuertos'] = 0
        self.variables_test['depredadores'] = 0
        self.variables_test['zonasAlimentacion'] = 0
        self.resetDataSet()
        self.construirMapa(21)

        if(accion == False):
            self.varibales_control['time_life'] = 0 
            self.varibales_control['tespera'] = 0  
            self.varibales_control['talimentacion'] = 0 
            self.varibales_control['numero_aparicion'] = 0  
            self.varibales_control['tasas_aparicion'] = 0 
            self.varibales_control['cantidad_recursos'] = 0 
            self.varibales_control['tamano_poblacion'] = 0
            self.varibales_control['reproducciones'] = 0 
            self.varibales_control['prob_explorador'] = 0 


    '''
        Verifica si un click fué dado en algún botón de los paneles y ejecuta su acción
        
        PARÁMETROS
        -panel: es el panel que se encuentra activo
        -event: el evento que ha sido capturado

        RETURN
        - devuelve el nombre del panel que debe ser activado segun corresponda la acción
        - None: en caso de que el click este por fuera de un campo de acción
    '''    
    def accionesBotones(self, panel, event):
        
        botones = []
        inputs = []
        back = ""
        nextt = ""
        if(panel == "entorno"):
            back = "opciones"
            nextt = "individuo"
            botones = self.lista_btn_entorno
            inputs = self.input_entorno

        elif(panel == "individuo"):
            back = "entorno"
            nextt = "alimentacion"
            botones = self.lista_btn_individuo
            inputs = self.input_individuos
        
        elif(panel == "alimentacion"):
            back = "individuo"
            nextt = "depredador"
            botones = self.lista_btn_alimentacion
            inputs = self.input_alimentacion
        
        elif(panel == "depredador"):
            back = "alimentacion"
            nextt = "simulacion"
            botones = self.lista_btn_depredador
            inputs = self.input_depredadores
            
        elif(panel == "opciones"):
            botones = self.lista_btn_opciones

        elif(panel == "inicio"):
            botones = self.lista_btn_inicio

        elif(panel == "simulacion"):
            botones = self.lista_btn_simulacion        
        
        for boton in botones:
            if (self.validar_clic(event.pos[0], event.pos[1], boton['left_top'][0],
                boton['left_top'][1], boton['right_bottom'][0], boton['right_bottom'][1])):

                    if(panel == "opciones"):
                        if(boton['name'] == 'btn_cargar'):
                            if(self.archivoCoreccto()):
                                print(self.varibales_control)
                                return "simulacion"
                            else:
                                return None

                            
                        if(boton['name'] == 'btn_digitar'):
                            return"entorno"
                    elif(panel == "inicio"):
                        if(boton['name'] == 'btn_next_inicio'):
                            return "opciones"
                    elif(panel == "simulacion"):
                        if(boton['name'] == 'btn_play'):
                                if(boton['show']):
                                    
                                    if(self.pause_simulacion):
                                        boton['show'] = False
                                        #if(res[0]):
                                        self.pause_simulacion = False
                                        self.download = True
                                else:
                                    
                                    boton['show'] = True
                                    self.pause_simulacion = True
                                                                       
                        
                        elif(boton['name']== 'btn_detener'):
                            self.pause_simulacion = True
                            if(self.download and self.generacion>0):
                                res = eg.ynbox('Do you want to save the data?', 'DataTest', ('Yes', 'No'))
                                if(res):
                                    self.almacenarDatos()
                                    
                                self.download = False
                            pygame.time.delay(self.velocidad_pause)
                            self.reiniciarVariables(False)
                            return "opciones"


                        elif(boton['name'] == 'btn_aumentar'):
                            if(self.velocidad == 0):
                                pass
                            else:
                                self.velocidad = self.velocidad - 200
                        elif(boton['name'] == 'btn_disminuir'):
                            if(self.velocidad == 1500):
                                pass
                            else:
                                self.velocidad = self.velocidad + 200
                        elif(boton['name'] == 'btn_reiniciar'):
                            self.pause_simulacion = True
                            pygame.time.delay(self.velocidad_pause)
                            self.reiniciarVariables(True)
                            self.cargarValoresIniciales()
                            return "simulacion"
                            
                        
                    else:
                        if(boton['name'] == 'btn_back'):
                            return back
                                                       
                        if(boton['name'] == 'btn_next'):
                            
                            if(self.inputsVacios(inputs)):
                                return None
                            
                            if(panel == 'depredador'):
                                self.cargarValoresIniciales()
                                return nextt
                            else:
                                return nextt
                            
        return None

    '''
        Verifica si el click fué dado en un input 

        PARÁMETROS
        -panel: es el panel que se encuentra activo
        -event: el evento que ha sido capturado
        
    '''    
    def inputMouseDown(self, panel, event):
        
        lista_inputs = []
        if(panel == "entorno"):
            lista_inputs = self.input_entorno

        elif(panel == "individuo"):
            lista_inputs = self.input_individuos
        
        elif(panel == "alimentacion"):
            lista_inputs = self.input_alimentacion
        
        elif(panel == "depredador"):
            lista_inputs = self.input_depredadores
        
        else:
            pass

        
        for campo in lista_inputs:
            
            if(self.validar_clic(event.pos[0], event.pos[1], campo['left_top'][0], 
                  campo['left_top'][1], campo['right_bottom'][0], campo['right_bottom'][1])):
                        if(campo['edit']):
                            self.textoDigitado = ""
                            campo['edit'] = False
                            
                            if(self.varibales_control[campo['name']]==''):
                                self.varibales_control[campo['name']] = 0
                        else:
                            campo['edit'] = True
                            self.textoDigitado = str(self.varibales_control[campo['name']])
            else:
                if(campo['edit']):
                    self.textoDigitado = ""
                    campo['edit'] = False
                    if(self.varibales_control[campo['name']]==''):
                            self.varibales_control[campo['name']] = 0
    
    '''
        Maneja la digitación de teclas en los inputs

        PARÁMETROS
        -panel: es el panel que se encuentra activo
        -event: el evento que ha sido capturado

        
    '''    
    def inputKey(self, panel):
        
        lista_inputs = []
        if(panel == "entorno"):
            lista_inputs = self.input_entorno

        elif(panel == "individuo"):
            lista_inputs = self.input_individuos
        
        elif(panel == "alimentacion"):
            lista_inputs = self.input_alimentacion
        
        elif(panel == "depredador"):
            lista_inputs = self.input_depredadores
        
        else:
            pass

        
        for campo in lista_inputs:
            if(campo['edit']):
                self.varibales_control[campo['name']]  = int(self.textoDigitado) if (self.textoDigitado != '') else ''
         
    '''
        Verifica si el mouse esta moviendose encima de un botón y 
        modifica el atributo del botón que indica que se debe activar para ser
        pintado posteriormente por la función dibijar_botones

        PARÁMETROS
        -panel: es el panel que se encuentra activo
        -event: el evento que ha sido capturado

        
    '''    
    def hooverBotones(self, panel, event):
        
        botones = []
        if(panel == "entorno"):
            botones = self.lista_btn_entorno

        elif(panel == "individuo"):
            botones = self.lista_btn_individuo
        
        elif(panel == "alimentacion"):
            botones = self.lista_btn_alimentacion
        
        elif(panel == "depredador"):
            botones = self.lista_btn_depredador

        elif(panel == "opciones"):
            botones = self.lista_btn_opciones
        
        elif(panel == "inicio"):
            botones = self.lista_btn_inicio

        elif(panel == "simulacion"):
            botones = self.lista_btn_simulacion
            
        
        
        for boton in botones:
            if (self.validar_clic(event.pos[0], event.pos[1], boton['left_top'][0],
                boton['left_top'][1], boton['right_bottom'][0], boton['right_bottom'][1])):

                    boton['mouse_motion'] = True
            else:
                boton['mouse_motion'] = False

        return None   
    '''
        Carga los valores iniciales de la simulación en el mapa
    '''  
    def cargarValoresIniciales(self):     
        if(self.recursosIniciales):
            self.zonasAlimentacionInicial()
            cantidad_depredadores = self.varibales_control['tamano_grupos']
            for i in range(cantidad_depredadores):
                self.crearDepredador()
            self.recursosIniciales = False 
                
        if(self.individuosIniciales):
            self.ubicacionIndividuosIniciales()
            self.algoritmoKMeans()
            pass

    def updateDataSet(self):
        cantNeutral = 0
        depredadoresNeutral = 0
        zonasNeutral = 0
        
        cantOptimistas = 0
        depredadoresOptimistas = 0
        zonasOptimistas = 0

        cantMuyOptimistas = 0
        depredadoresMuyOptimistas = 0
        zonasMuyOptimistas = 0

        cantPesimistas = 0
        depredadoresPesimistas = 0
        zonasPesimistas = 0

        cantMuyPesimistas = 0
        depredadoresMuyPesimistas = 0
        zonasMuyPesimistas = 0

        for individuo in self.individuos_array:
            if(individuo['vida']>0):
                if(individuo['personalidad'][2] == "Neutral"):
                    cantNeutral += 1
                    zonasNeutral += len(individuo['zonas_alimentacion'])
                    depredadoresNeutral += len(individuo['zonas_depredadores'])
                if(individuo['personalidad'][2] == "Optimista"):
                    cantOptimistas += 1
                    zonasOptimistas += len(individuo['zonas_alimentacion'])
                    depredadoresOptimistas += len(individuo['zonas_depredadores'])
                if(individuo['personalidad'][2] == "Muy Optimista"):
                    cantMuyOptimistas += 1
                    zonasMuyOptimistas += len(individuo['zonas_alimentacion'])
                    depredadoresMuyOptimistas += len(individuo['zonas_depredadores'])
                if(individuo['personalidad'][2] == "Pesimista"):
                    cantPesimistas += 1
                    zonasPesimistas += len(individuo['zonas_alimentacion'])
                    depredadoresPesimistas += len(individuo['zonas_depredadores'])
                if(individuo['personalidad'][2] == "Muy Pesimista"):
                    cantMuyPesimistas += 1
                    zonasMuyPesimistas += len(individuo['zonas_alimentacion'])
                    depredadoresMuyPesimistas += len(individuo['zonas_depredadores'])
        self.variables_test['neutralConocimientoZonas'] = ("{0:.4f}".format(zonasNeutral/cantNeutral)) if (cantNeutral > 0) else 0.0
        self.variables_test['neutralConocimientoDepredadores'] = ("{0:.4f}".format(depredadoresNeutral/cantNeutral)) if (cantNeutral > 0) else 0.0
        self.variables_test['pesimistaConocimientoZonas'] = ("{0:.4f}".format(zonasPesimistas/cantPesimistas)) if (cantPesimistas > 0) else 0.0
        self.variables_test['pesimistaConocimientoDepredadores'] = ("{0:.4f}".format(depredadoresPesimistas/cantPesimistas)) if (cantPesimistas > 0) else 0.0
        self.variables_test['MuyPesimistaConocimientoZonas'] = ("{0:.4f}".format(zonasMuyPesimistas/cantMuyPesimistas)) if (cantMuyPesimistas > 0) else 0.0
        self.variables_test['MuyPesimistaConocimientoDepredadores'] = ("{0:.4f}".format(depredadoresMuyPesimistas/cantMuyPesimistas)) if (cantMuyPesimistas > 0) else 0.0
        self.variables_test['optimistaConocimientoZonas'] = ("{0:.4f}".format(zonasOptimistas/cantOptimistas)) if (cantOptimistas > 0) else 0.0
        self.variables_test['optimistaConocimientoDepredadores'] = ("{0:.4f}".format(depredadoresOptimistas/cantOptimistas)) if (cantOptimistas > 0) else 0.0
        self.variables_test['muyOptimistaConocimientoZonas'] = ("{0:.4f}".format(zonasMuyOptimistas/cantMuyOptimistas)) if (cantMuyOptimistas > 0) else 0.0
        self.variables_test['muyOptimistaConocimientoDepredadores'] = ("{0:.4f}".format(depredadoresMuyOptimistas/cantMuyOptimistas)) if (cantMuyOptimistas > 0) else 0.0
        # print(zonasNeutral, depredadoresNeutral, cantNeutral)
        # print(zonasNeutral//cantNeutral)
        # print(depredadoresNeutral//cantNeutral)
        self.data_set.append(self.variables_test.values())
        self.resetDataSet()
    
    
    """
        Para optimizar la función nuevoDestino se realiza una depuración del conocimiento de los individuos
        cada 3 generaciones, es decir se eliminan aquellas zonas que su ubicación fue proporcionada por otro
        individuo y aun no se ha comprobado la existencia de dicha zona es decir el informante es diferente a -1
    """
    def depurarInformacion(self):
        for individuo in self.individuos_array:
            for x in individuo['zonas_alimentacion']:
                if(x[6] != -1):
                    if(x[5] == False):
                        individuo['zonas_alimentacion'].remove(x)
    
    '''
        DESCRIPCIÓN FUNCIÓN
    '''    
    def iniciarSimulacion(self):

        clock = pygame.time.Clock()
        #Validacion de input, teclas permitidas
        keys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'backspace']

        #almacena el panel que se encuentra activo en la ventana
        panel_activo = "inicio"
        
        # Se crean las listas de botones de cada panel
        self.cargar_img_botones()
        # Se crean las listas de inputs de cada panel
        self.cargarInputs()
        
        self.construirMapa(21)
        
        self.tasaAparicionZonas = 0
        while True:
            
            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

                if event.type == pygame.MOUSEBUTTONDOWN:
                                        
                    #se valida si el click es una acción en el boton back o next del panel activo
                    cambiar_panel = self.accionesBotones(panel_activo, event)
                    panel_activo = panel_activo if cambiar_panel is None else cambiar_panel
                    
                    # manejo del click de los inputs
                    self.inputMouseDown(panel_activo,event)

                    self.mostrarInformacion(event.pos[0], event.pos[1])
                   
                   
                if event.type in (pygame.KEYDOWN, pygame.KEYUP):
                    # obtiene el nombre de la tecla
                    key_name = pygame.key.name(event.key)
                    key_name = key_name.replace('[','').replace(']','') if ('[' in key_name) else key_name 
                    
                    # si alguna tecla es presionada
                    if event.type == pygame.KEYDOWN:
                        if(key_name in keys):
                            if(key_name == 'backspace'):
                                tamanioTexto = len(self.textoDigitado)
                                self.textoDigitado = self.textoDigitado[:tamanioTexto-1]
                            else:
                                if(self.textoDigitado=='0'):
                                    self.textoDigitado = key_name
                                else:
                                    self.textoDigitado += key_name
                            self.inputKey(panel_activo)
                          
                        #! *************

                if event.type == pygame.MOUSEMOTION:
                    #! Esto se modificó 
                    #self.mostrarInformacion(event.pos[0], event.pos[1])
                    self.hooverBotones(panel_activo,event)

            # Cargamos el panel activo en la ventana
            self.mostrarPanel(panel_activo)

            if(panel_activo != "inicio"):
                # Dibujo del rectángulo, en este espacio se ubicará el mapa
                #pygame.draw.rect(self.ventana, (97, 236, 47), (0, 0, 630, self.height), 0)
                self.dibujarMapa(21)

            # Se simula el movimiento del individuo
            if(not self.pause_simulacion):
                #self.mostrarIndividuosAll()
                if(self.generacion%3==0):
                    self.depurarInformacion()
                self.moverIndividuos(self.individuos_array)
                self.zonasCercanasAlimentacion(self.individuos_array)
                self.zonasCercanasDepredadores(self.individuos_array)
                self.asignarEmocion()
                self.negociacion()
                self.calcularMaximoExploradores()
                self.reproduccionIndividuos()
                self.alimentarDepredador()
                self.disminuirVida()
                self.individuos_array += self.hijos_array
                self.actualizarPosicionHijos()
                self.hijos_array = []
                self.actualizacionInformacionLocal()
                self.actualizacionZonasAlimentacion()
                #Esta funcion actualiza el destino de los individuos que no tienen una zona de alimentacion cercana
                self.miNuevaZona() 
                self.generacion += 1
                self.tasaAparicionZonas += 1
                self.tasaAparicionDepredadores +=1
                if(self.tasaAparicionZonas == int(self.varibales_control['tasas_aparicion'])):
                    self.zonasAlimentacionInicial()
                    self.tasaAparicionZonas = 0
                if(self.tasaAparicionDepredadores == int(self.varibales_control['numero_aparicion'])):
                    cantidad_depredadores = self.varibales_control['tamano_grupos']
                    for i in range(cantidad_depredadores):
                        self.crearDepredador()
                    self.tasaAparicionDepredadores = 0
                self.actualizarMeReproduzco()
                self.updateDataSet()
            
            if(panel_activo == 'simulacion'):   
                # Se pinta el label de la generacion
                Fuente = pygame.font.Font(os.path.join(self.fuentes_path,"Montserrat-Bold.ttf"), 80)
                text_gen = Fuente.render(str(self.generacion), True, (0, 0, 0))
                
                # create a rectangular object for the 
                # text surface object 
                textRect = text_gen.get_rect()  
                
                # set the center of the rectangular object. 
                textRect.center = (651 + (549 // 2), 277) 
                self.ventana.blit(text_gen, textRect)
            pygame.display.update()
            
            if(panel_activo != "simulacion"):
                clock.tick(30)
            
            else:
                pygame.time.delay(self.velocidad)
            
    def main(self):
        self.iniciarSimulacion()
